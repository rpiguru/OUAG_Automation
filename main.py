import os

os.environ['QT_API'] = 'pyside2'

import threading
import time
import cv2
import sys
from functools import partial
from widgets.image import ImageWidget
from widgets.control_dialog import ControlDialog
from settings import SHOW_CURSOR, GAUGE_CONF, SNAP_INTERVALS, SHOP_URL
from utils.logger import logger
from utils.sensor import read_sensor_data
from utils.common import kill_process_by_name, disable_screen_saver, get_config, is_rpi, update_config_file, \
    check_running_proc, scan_wifi_networks, create_wifi_config
from utils.relay_manager import RelayManager
from PySide2.QtWidgets import QMainWindow, QApplication, QMessageBox, QLineEdit
from widgets.dialogs.keyboard import KeyboardDlg
import subprocess

from utils.camera import CameraManager
from widgets.image_viewer import ImageViewerDialog
from widgets.message import show_message

from PySide2.QtCore import Qt, Signal, QTimer
from PySide2.QtGui import QColor
from PySide2.QtCharts import QtCharts
from widgets.gauge import AnalogGaugeWidget
from ui.ui_ouag import Ui_OUAGWindow


class OUAGApp(QMainWindow):

    photo_path = Signal(str)
    sensor_data_signal = Signal(list)

    def __init__(self):
        super().__init__()
        self.ui = Ui_OUAGWindow()
        self.ui.setupUi(self)
        if not SHOW_CURSOR:
            self.setCursor(Qt.BlankCursor)
        if is_rpi:
            self.move(0, 0)
            self.setWindowFlag(Qt.FramelessWindowHint)
        for k in {'light', 'exhaust', 'humidity', 'air_fan'}:
            getattr(self.ui, f"btn_{k}_control").released.connect(partial(self._on_btn_control, k))
        self.gauges = {}
        for k in {'ph', 'ec', 'temp', 'humidity', 'water_temp'}:
            c = GAUGE_CONF[k]
            self.gauges[k] = AnalogGaugeWidget(
                parent=self, min_value=c['min'], max_value=c['max'], colors=c['colors'], label_count=c['label_count'])
            getattr(self.ui, f"layout_{k}").addWidget(self.gauges[k])
        for k in {'home', 'shop', 'camera', 'settings', 'system_update', 'software_update', 'chart', 'take_photo',
                  'photo_archive', 'refresh', "refresh_ssid", 'input_password', 'connect_wifi'}:
            getattr(self.ui, f"btn_{k}").released.connect(partial(self._on_btn, k))
        self.cur_img_widget = ImageWidget(parent=self)
        self.cur_img_widget.setFixedSize(self.ui.image_frame.size())
        self.ui.layoutImage.addWidget(self.cur_img_widget)
        self.sensor_data = []
        getattr(self, 'photo_path').connect(self._on_photo_taken)
        getattr(self, 'sensor_data_signal').connect(self._on_sensor_data)
        self.ui.combo_sensor.currentIndexChanged.connect(lambda: self._draw_chart())
        self.ui.snap_interval.setCurrentIndex(SNAP_INTERVALS.index(get_config()['snap_interval']))
        self.ui.snap_interval.currentTextChanged.connect(self._on_snap_interval_changed)
        self.cam = CameraManager()
        self.cam.start()
        self.relay = RelayManager()
        threading.Thread(target=self._read_sensor_data).start()
        self._browser_timer = QTimer(self)
        self._browser_timer.setInterval(1000)
        self._browser_timer.timeout.connect(self._check_browser)
        self.dlg_keyboard = None
        self._cur_widget = None
        for w in self.findChildren(QLineEdit):
            w.mousePressEvent = partial(self._on_line_focused, w)

    def _on_btn_control(self, c_type):
        dlg = ControlDialog(parent=self, ctrl=c_type)
        dlg.open()

    def _on_btn(self, b_type):
        if b_type in {'home', 'camera', 'settings'}:
            self.ui.stack_widget.setCurrentWidget(getattr(self.ui, f"page_{b_type}"))
        elif b_type == 'chart':
            self.ui.stack_widget.setCurrentWidget(self.ui.page_chart)
            self._draw_chart()
        elif b_type == 'take_photo':
            self.ui.btn_take_photo.setEnabled(False)
            threading.Thread(target=self._take_photo).start()
        elif b_type == 'photo_archive':
            dlg = ImageViewerDialog(parent=self)
            dlg.open()
        elif b_type == 'system_update':
            if is_rpi:
                p = subprocess.Popen('sudo apt update', shell=True, stdout=subprocess.PIPE, stderr=subprocess.PIPE)
                stdout, stderr = p.communicate()
                logger.debug(f"System Update, out: {stdout}, err: {stderr}")
            else:
                time.sleep(3)
            show_message("Updated System!", auto_close_time=3)
        elif b_type == 'software_update':
            if is_rpi:
                p = subprocess.Popen('git pull', shell=True, stdout=subprocess.PIPE, stderr=subprocess.PIPE)
                stdout, stderr = p.communicate()
                logger.debug(f"Git Update, out: {stdout}, err: {stderr}")
                if 'Fast-forward' in stdout.decode():
                    reply = QMessageBox.question(self, 'OUAG', "Updated software, reboot?",
                                                 QMessageBox.Yes, QMessageBox.No)
                    if reply == QMessageBox.Yes:
                        os.system('sudo reboot')
                else:
                    show_message("Already updated to the latest!", auto_close_time=3)
        elif b_type == 'refresh':
            new_time = time.time() - get_config()['interval'] - 10
            update_config_file({'last_reading_time': new_time})
            # Wait for 2 sec so that backend can read the sensor data and save to DB
            time.sleep(2)
        elif b_type == 'shop':
            logger.info(f"Opening browser - {SHOP_URL}")
            subprocess.Popen(f'chromium-browser {SHOP_URL}', shell=True)
            self.hide()
            self._browser_timer.start()
        elif b_type == 'refresh_ssid':
            ap_list = scan_wifi_networks()
            combo = self.ui.ssid
            cur_ssid = combo.currentText()
            combo.clear()
            combo.addItems(ap_list)
            if cur_ssid and cur_ssid in ap_list:
                combo.setCurrentText(cur_ssid)
        elif b_type == 'input_password':
            self.ui.password.setFocus()
            self._cur_widget = self.ui.password
            self.dlg_keyboard = KeyboardDlg(parent=self)
            self.dlg_keyboard.pressed.connect(self._on_key_pressed)
            self.dlg_keyboard.show()
        elif b_type == 'connect_wifi':
            ssid = self.ui.ssid.currentText()
            if ssid:
                pwd = self.ui.password.text()
                logger.info(f"Connecting to AP - {ssid}")
                create_wifi_config(ssid=ssid, password=pwd)
                subprocess.Popen("sudo service dnsmasq restart && sudo service dhcpcd restart", shell=True)
            else:
                show_message("Please select an SSID!", msg_type='Warning')

    def _on_key_pressed(self, k):
        if isinstance(self._cur_widget, QLineEdit):
            txt = self._cur_widget.text()
            if k == 'backspace':
                txt = txt[:-1]
            else:
                txt += k
            self._cur_widget.setText(txt)

    def _on_line_focused(self, *args):
        self._cur_widget = args[0]

    def _check_browser(self):
        if not check_running_proc('chromium-browser'):
            logger.debug(f"Browser closed, restoring now...")
            self._browser_timer.stop()
            self.show()

    def _read_sensor_data(self):
        while True:
            sensor_data = read_sensor_data()
            getattr(self, 'sensor_data_signal').emit(sensor_data)
            time.sleep(10)

    def _on_sensor_data(self, sensor_data):
        self.sensor_data = sensor_data
        cur_page = self.ui.stack_widget.currentWidget().objectName()
        if cur_page == 'page_home':
            if sensor_data:
                for k in {'ph', 'ec', 'temp', 'humidity', 'water_temp'}:
                    getattr(self.ui, f"cur_{k}").setText(str(sensor_data[0][k]))
                    getattr(self.ui, f"ts_{k}").setText(sensor_data[0]['ts'].strftime('%m/%d/%y %H:%M:%S'))
                    self.gauges[k].update_value(sensor_data[0][k])
        elif cur_page == 'page_chart':
            self._draw_chart()

    def _draw_chart(self):
        for i in reversed(range(self.ui.layout_chart.count())):
            item = self.ui.layout_chart.itemAt(i)
            if item.widget() is not None:
                item.widget().deleteLater()
        chartview = QtCharts.QChartView()
        chartview.setContentsMargins(0, 0, 0, 0)
        self.ui.layout_chart.addWidget(chartview)
        chart = QtCharts.QChart()
        cur_sensor = ['ph', 'ec', 'temp', 'water_temp', 'humidity'][self.ui.combo_sensor.currentIndex()]
        series = QtCharts.QLineSeries()
        series.setColor(QColor(["blue", "green", "red", "magenta", "darkCyan"][self.ui.combo_sensor.currentIndex()]))
        series.setName(
            {'ph': 'pH', 'ec': 'EC', 'temp': 'Temp', 'humidity': 'Humidity', 'water_temp': 'Water Temp'}[cur_sensor])
        for v in self.sensor_data[::-1]:
            series.append(v['ts'].timestamp() * 1000, v[cur_sensor])
        chart.addSeries(series)
        chart.setAnimationOptions(QtCharts.QChart.SeriesAnimations)

        axis_x = QtCharts.QDateTimeAxis()
        chart.addAxis(axis_x, Qt.AlignBottom)
        series.attachAxis(axis_x)
        axis_x.setTickCount(10)
        axis_x.setLabelsAngle(70)
        axis_x.setFormat("hh:mm")
        axis_x.setTitleText("DateTime")
        axis_y = QtCharts.QValueAxis()
        axis_y.setLabelFormat('%i')
        axis_y.setMax(GAUGE_CONF.get(cur_sensor, {}).get('max', 100))
        axis_y.setMin(GAUGE_CONF.get(cur_sensor, {}).get('min', 0))
        chart.addAxis(axis_y, Qt.AlignLeft)
        series.attachAxis(axis_y)
        chartview.setChart(chart)

    def _take_photo(self):
        if is_rpi:
            file_name = self.cam.take_photo()
        else:
            time.sleep(2)
            file_name = "ui/img/greenhouse.jpg"
        getattr(self, 'photo_path').emit(file_name)

    def _on_snap_interval_changed(self):
        snap_interval = SNAP_INTERVALS[self.ui.snap_interval.currentIndex()]
        update_config_file({'snap_interval': snap_interval})

    def _on_photo_taken(self, path):
        self.ui.btn_take_photo.setEnabled(True)
        self.cur_img_widget.set_frame(cv2.imread(path))

    def closeEvent(self, event):
        self.cam.stop()
        super().closeEvent(event)


if __name__ == '__main__':

    app = QApplication(sys.argv)
    ex = OUAGApp()

    sys._excepthook = sys.excepthook

    def exception_hook(exctype, value, tb):
        logger.error("=========== Crashed!", exc_info=(exctype, value, tb))
        getattr(sys, "_excepthook")(exctype, value, tb)
        ex.on_crashed()
        sys.exit(1)

    sys.excepthook = exception_hook

    logger.info('========== Starting OUAG Application ==========')

    if is_rpi:
        kill_process_by_name("fbi", use_sudo=True)

    disable_screen_saver()

    ex.show()
    sys.exit(app.exec_())
