import glob
import ntpath
import os

from PySide2.QtCore import Qt, QTimer
from PySide2.QtGui import QPixmap

from settings import IMAGE_DIR
from ui.ui_image_viewer_dlg import Ui_ImageViewerDialog
from widgets.dialogs.base import DialogBase
from widgets.image_widget import PhotoViewer


class ImageViewerDialog(DialogBase):

    def __init__(self, parent):
        super().__init__(parent, Qt.WindowSystemMenuHint | Qt.WindowTitleHint | Qt.WindowCloseButtonHint)
        self.ui = Ui_ImageViewerDialog()
        self.ui.setupUi(self)
        self.ui.btn_next.released.connect(self._on_btn_next)
        self.ui.btn_previous.released.connect(self._on_btn_previous)
        self.index = 0
        self.images = glob.glob(os.path.join(IMAGE_DIR, "*.jpg"))
        self.viewer = PhotoViewer(parent=self)
        self.ui.layout.addWidget(self.viewer)
        QTimer.singleShot(100, self._load_image)

    def _on_btn_next(self):
        self.index = (self.index + 1) if self.index < len(self.images) - 1 else 0
        self._load_image()

    def _on_btn_previous(self):
        self.index = (self.index - 1) if self.index > 0 else len(self.images) - 1
        self._load_image()

    def _load_image(self):
        if self.images:
            try:
                path = self.images[self.index]
                self.ui.name.setText(ntpath.basename(path))
                self.viewer.set_photo(QPixmap(path))
            except IndexError:
                pass
