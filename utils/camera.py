import os
from datetime import datetime
import threading
import time

from settings import IMAGE_DIR
from utils.common import get_config, is_rpi
from utils.logger import logger


if is_rpi:
    import picamera


class CameraManager(threading.Thread):

    def __init__(self):
        super().__init__()
        self._b_stop = threading.Event()
        self._b_stop.clear()
        self._lock = threading.Lock()

    def run(self) -> None:
        last_snap_time = time.time()
        while not self._b_stop.is_set():
            snap_interval = get_config()['snap_interval']
            if snap_interval is not None:
                if time.time() - last_snap_time > snap_interval * 60:
                    if is_rpi:
                        self.take_photo()
            time.sleep(1)

    def take_photo(self):
        with self._lock:
            with picamera.PiCamera() as camera:
                camera.resolution = (1024, 768)
                camera.start_preview()
                # Camera warm-up time
                time.sleep(2)
                file_name = os.path.join(IMAGE_DIR, f"{datetime.now().strftime('%m-%d-%Y--%H-%M-%S')}.jpg")
                camera.capture(file_name)
        logger.info(f"Took photo - {file_name}")
        return file_name

    def stop(self):
        self._b_stop.set()
