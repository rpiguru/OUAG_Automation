from settings import PUMP_PINS
from utils.common import is_rpi
from utils.logger import logger

if is_rpi:
    import RPi.GPIO as GPIO
    GPIO.setwarnings(False)
    GPIO.setmode(GPIO.BCM)


class RelayManager(object):

    def __init__(self):
        self._state = {}
        for k, pin in PUMP_PINS.items():
            if is_rpi:
                GPIO.setup(pin, GPIO.OUT)
            self.turn_pump(k, False)

    def turn_pump(self, name, val):
        self._state[name] = val
        logger.info(f"Turning {name} {'ON' if val else 'OFF'}")
        if is_rpi:
            GPIO.output(PUMP_PINS[name], GPIO.HIGH if val else GPIO.LOW)

    def get_state(self, name):
        return self._state.get(name, False)
