from functools import partial

from PySide2.QtCore import Signal

from ui.ui_keyboard import Ui_KeyboardDlg
from utils.common import is_rpi
from widgets.dialogs.base import DialogBase


KEYS = [
    [
        ' ', '`', '1', '2', '3', '4', '5', '6', '7', '8', '9', '0', '-', '=', '⌫',
        'q', 'w', 'e', 'r', 't', 'y', 'u', 'i', 'o', 'p', '[', ']', '\\',
        'caps', 'a', 's', 'd', 'f', 'g', 'h', 'j', 'k', 'l', ';', '\'', 'enter',
        'shift', 'z', 'x', 'c', 'v', 'b', 'n', 'm', ',', '.', '/', 'shift'
    ],
    [
        ' ', '~', '!', '@', '#', '$', '%', '^', '&&', '*', '(', '(', '_', '+', '⌫',
        'Q', 'W', 'E', 'R', 'T', 'Y', 'U', 'I', 'O', 'P', '{', '}', '|',
        'caps', 'A', 'S', 'D', 'F', 'G', 'H', 'J', 'K', 'L', ':', '"', 'enter',
        'shift', 'Z', 'X', 'C', 'V', 'B', 'N', 'M', '<', '>', '?', 'shift'
    ],
]


class KeyboardDlg(DialogBase):

    pressed = Signal(str)

    def __init__(self, parent=None):
        super().__init__(parent, is_fullscreen=False)
        self.index = 0
        self.ui = Ui_KeyboardDlg()
        self.ui.setupUi(self)
        if is_rpi:
            self.setGeometry(112, 360, 800, 240)
        for i in range(52):
            getattr(self.ui, f"btn_{i}").released.connect(partial(self._on_btn, i))

    def _on_btn(self, i):
        k = KEYS[self.index][i]
        if k == 'caps':
            self.index = 0 if self.index == 1 else 1
            for i in range(52):
                getattr(self.ui, f"btn_{i}").setText(KEYS[self.index][i])
        elif k in {'shift', 'enter'}:
            pass
        elif k == '⌫':
            self.pressed.emit('backspace')
        else:
            self.pressed.emit(k)
