# -*- coding: utf-8 -*-

################################################################################
## Form generated from reading UI file 'image_viewer_dlg.ui'
##
## Created by: Qt User Interface Compiler version 5.15.1
##
## WARNING! All changes made in this file will be lost when recompiling UI file!
################################################################################

from PySide2.QtCore import *
from PySide2.QtGui import *
from PySide2.QtWidgets import *

import ui.ouag_rc

class Ui_ImageViewerDialog(object):
    def setupUi(self, ImageViewerDialog):
        if not ImageViewerDialog.objectName():
            ImageViewerDialog.setObjectName(u"ImageViewerDialog")
        ImageViewerDialog.resize(824, 575)
        ImageViewerDialog.setMinimumSize(QSize(640, 480))
        ImageViewerDialog.setMaximumSize(QSize(6400, 4800))
        self.verticalLayout = QVBoxLayout(ImageViewerDialog)
        self.verticalLayout.setObjectName(u"verticalLayout")
        self.frame = QFrame(ImageViewerDialog)
        self.frame.setObjectName(u"frame")
        sizePolicy = QSizePolicy(QSizePolicy.Preferred, QSizePolicy.Expanding)
        sizePolicy.setHorizontalStretch(0)
        sizePolicy.setVerticalStretch(0)
        sizePolicy.setHeightForWidth(self.frame.sizePolicy().hasHeightForWidth())
        self.frame.setSizePolicy(sizePolicy)
        self.frame.setMinimumSize(QSize(640, 480))
        self.frame.setFrameShape(QFrame.StyledPanel)
        self.frame.setFrameShadow(QFrame.Raised)
        self.layout = QVBoxLayout(self.frame)
        self.layout.setObjectName(u"layout")

        self.verticalLayout.addWidget(self.frame)

        self.horizontalLayout = QHBoxLayout()
        self.horizontalLayout.setSpacing(20)
        self.horizontalLayout.setObjectName(u"horizontalLayout")
        self.btn_previous = QToolButton(ImageViewerDialog)
        self.btn_previous.setObjectName(u"btn_previous")
        self.btn_previous.setMinimumSize(QSize(40, 40))
        icon = QIcon()
        icon.addFile(u":/img/img/left-arrow-2561.png", QSize(), QIcon.Normal, QIcon.Off)
        self.btn_previous.setIcon(icon)
        self.btn_previous.setIconSize(QSize(35, 35))

        self.horizontalLayout.addWidget(self.btn_previous)

        self.name = QLabel(ImageViewerDialog)
        self.name.setObjectName(u"name")
        sizePolicy1 = QSizePolicy(QSizePolicy.Expanding, QSizePolicy.Preferred)
        sizePolicy1.setHorizontalStretch(0)
        sizePolicy1.setVerticalStretch(0)
        sizePolicy1.setHeightForWidth(self.name.sizePolicy().hasHeightForWidth())
        self.name.setSizePolicy(sizePolicy1)
        font = QFont()
        font.setFamily(u"Comic Sans MS")
        font.setPointSize(12)
        self.name.setFont(font)
        self.name.setAlignment(Qt.AlignCenter)

        self.horizontalLayout.addWidget(self.name)

        self.btn_next = QToolButton(ImageViewerDialog)
        self.btn_next.setObjectName(u"btn_next")
        self.btn_next.setMinimumSize(QSize(40, 40))
        icon1 = QIcon()
        icon1.addFile(u":/img/img/right-arrow-2561.png", QSize(), QIcon.Normal, QIcon.Off)
        self.btn_next.setIcon(icon1)
        self.btn_next.setIconSize(QSize(35, 35))

        self.horizontalLayout.addWidget(self.btn_next)


        self.verticalLayout.addLayout(self.horizontalLayout)


        self.retranslateUi(ImageViewerDialog)

        QMetaObject.connectSlotsByName(ImageViewerDialog)
    # setupUi

    def retranslateUi(self, ImageViewerDialog):
        ImageViewerDialog.setWindowTitle(QCoreApplication.translate("ImageViewerDialog", u"Photo Archive", None))
        self.btn_previous.setText(QCoreApplication.translate("ImageViewerDialog", u"...", None))
        self.name.setText("")
        self.btn_next.setText(QCoreApplication.translate("ImageViewerDialog", u"...", None))
    # retranslateUi

