import ntpath
import os
from datetime import datetime
from functools import partial

from PySide2.QtCore import Signal
from PySide2.QtWidgets import QWidget

from settings import is_10_inch
from ui.ui_image_item import Ui_ImageItem
from ui.ui_image_item_7 import Ui_ImageItem7


class MediaItem(QWidget):

    action = Signal(str)

    def __init__(self, parent, media_type, path="", allow_delete=False):
        super().__init__(parent)
        self.ui = Ui_ImageItem() if is_10_inch else Ui_ImageItem7()
        self.ui.setupUi(self)
        self.media_type = media_type
        self.path = path
        self.ui.lbName.setText(ntpath.basename(path))
        self.ui.lbDatetime.setText(datetime.fromtimestamp(os.path.getmtime(path)).strftime("%m/%d/%Y %H:%M %p"))
        if not allow_delete:
            self.ui.btnDelete.setEnabled(False)
        self.ui.btnSave.released.connect(partial(self.action.emit, "save"))
        self.ui.btnView.released.connect(partial(self.action.emit, "view"))
        self.ui.btnDelete.released.connect(partial(self.action.emit, "delete"))
