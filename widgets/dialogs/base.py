from PySide2.QtCore import Signal, Qt, QRect
from PySide2.QtWidgets import QDialog

from settings import SHOW_CURSOR, SCREEN_SIZE


class DialogBase(QDialog):

    closed = Signal()

    def __init__(self, parent, is_fullscreen=True):
        super().__init__(parent)
        if not SHOW_CURSOR:
            self.setCursor(Qt.BlankCursor)
        self.is_fullscreen = is_fullscreen

    def exec_(self, *args, **kwargs):
        if self.is_fullscreen:
            self.setGeometry(QRect(0, 0, SCREEN_SIZE[0], SCREEN_SIZE[1]))
        super(DialogBase, self).exec_(*args, **kwargs)

    def closeEvent(self, arg__1):
        self.closed.emit()
