from PySide2.QtCore import Qt
from PySide2.QtWidgets import QDialog, QSpacerItem, QSizePolicy

from ui.ui_dlg_ctrl import Ui_ControlDialog
from widgets.toggle_button import SwitchButton

CONTROL_NAMES = {
    'light': "Turn Light",
    "exhaust": "Exhaust Fan",
    "humidity": "Humidifier",
    "air_fan": "Air Flow Fan"
}

DESCRIPTIONS = {
    'light': "Make sure that your dials are turned to the appropate Veg and "
             "Bloom percentages throughout the grow process.",
    "exhaust": "Exhaust fans should be left on always. This helps to keep fresh air coming into the cabinet. \n"
               "Only turn off exhaust fans for short periods of time. \n"
               "(Ex. Tent is too cool so you want to retain heat)",
    "humidity": "The humidifier is set to auto shut off when the humidity is within an acceptable range. \n"
                "Once the range has fallen to a minimum the system should auto turn on the humidifier. \n"
                "This control is so you can override the system for more control, or repair.",
    "air_fan": "Air flow is vital to your grow. In addition to the exhaust fans, all cabinets have an air flow fan.\n"
               "This fan helps to move the air inside the tent around, ensuring good air movement, \n"
               "Which is important to help eliminate mold and other nasty bugs from the crop."
}


class ControlDialog(QDialog):

    def __init__(self, parent, ctrl):
        super().__init__(parent, Qt.WindowSystemMenuHint | Qt.WindowTitleHint | Qt.WindowCloseButtonHint)
        self.relay = parent.relay
        self.ui = Ui_ControlDialog()
        self.ui.setupUi(self)
        self.ctrl = ctrl
        self.setWindowTitle(f"{ctrl.capitalize()} Control")
        self.ui.lb_control.setText(CONTROL_NAMES[ctrl])
        self.ui.lb_description.setText(DESCRIPTIONS[ctrl])
        if ctrl != 'light':
            self.ui.widget_table.deleteLater()
        else:
            pass

        btn = SwitchButton(parent=self, value=self.relay.get_state(self.ctrl))
        btn.state.connect(self._on_toggle_button)
        self.ui.layout_button.addWidget(btn)
        self.ui.layout_button.addItem(QSpacerItem(40, 20, QSizePolicy.Expanding, QSizePolicy.Minimum))
        self.ui.btn_close.released.connect(self.close)

    def _on_toggle_button(self, val):
        self.relay.turn_pump(self.ctrl, val)
