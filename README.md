# OUAG_Automation

OUAG Automation Project on Raspberry Pi

## Components

1. Ceramic Diffuser (plugged into smart plug) 

    https://www.amazon.com/dp/B003A7ZMHG

2. Exhaust Fan (plugged into smart plug) 

    https://www.amazon.com/dp/B009OWRMZ6

3. 7" Touchscreen from Waveshare 

    https://www.amazon.com/Capacitive-HDMI-LCD-Touchscreen-BeagleBone/dp/B07P7NZZ3F

4. Pi Camera (connected to RPi) 

    https://www.amazon.com/dp/B07W6THFPH

5. Dosing Pumps (5 of these connected to RPi) 
    
    https://www.amazon.com/dp/B01IUVHB8E
    
    Relay: https://www.amazon.com/dp/B00KTELP3I/ref=cm_sw_r_cp_apa_i_WssaGb82RDVPV

6. TDS / PPM Sensor (connected to RPi) 
    
    https://www.amazon.com/dp/B08KXRHK7H

    Connector for TDS : https://www.amazon.com/dp/B08KFZ3PVT

7. pH Sensor 

    https://www.amazon.com/dp/B08DRFDSLX

8. Water Temperature Sensor 
    
    https://www.amazon.com/dp/B01LY53CED

9. Air Temp/Humidity/Pressure 
    
    https://www.amazon.com/Tangyy-GY-BME280-Atmospheric-Environmental-Temperature/dp/B08QHRWB6H

10. ZigBee Smart Plug 
    
    https://www.amazon.com/dp/B08K45W4LR

11. XBee Antenna 

    https://www.amazon.com/dp/B007R9U1QA

12. Explorer Dongle 

    https://www.amazon.com/dp/B011QJIHY4

13. LED Light (smart plug) 

    https://www.amazon.com/dp/B07YWZF5ZP

14. USB Zigbee dongle

    https://www.amazon.com/dp/B01GJ826F8


## Wiring Components

- Breadboard View

    ![Breadboard View](schematic/ouag_bb.jpg)

- Schematic View

    ![Schematic View](schematic/ouag_schem.jpg)

## Software Installation

- Flash your micro SD card with **Raspberry Pi OS with desktop** image from [here](https://www.raspberrypi.org/software/operating-systems/)
 
- Clone this repository.

    ```shell script
    sudo apt update
    sudo apt install -y git
    cd ~
    git clone https://gitlab.com/rpiguru/OUAG_Automation
    ```
- Install everything:

    ```shell script
    cd OUAG_Automation
    bash setup.sh
    ```

- And reboot! :)
