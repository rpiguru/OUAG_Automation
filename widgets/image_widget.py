import sys

from PySide2.QtCore import QPoint, QRectF, Qt, Signal
from PySide2.QtGui import QBrush, QColor, QPixmap
from PySide2.QtWidgets import QApplication, QFrame, QGraphicsPixmapItem, QGraphicsScene, QGraphicsView, QHBoxLayout, \
    QPushButton, QVBoxLayout, QWidget


class ImageViewer(QWidget):

    def __init__(self, images=None, index=0):
        super(ImageViewer, self).__init__()
        self.images = images
        self.index = index
        self.viewer = PhotoViewer(self)
        self.setWindowFlags(Qt.WindowStaysOnTopHint)

        self.viewer.photoClicked.connect(self.photo_clicked)

        v_blayout = QVBoxLayout(self)
        v_blayout.addWidget(self.viewer)
        v_blayout.setMargin(0)

        if images:
            btn_layout = QHBoxLayout()
            self.btnBack = QPushButton('<')
            self.btnBack.clicked.connect(self.last_image)

            self.btnFoward = QPushButton('>')
            self.btnFoward.clicked.connect(self.next_image)

            btn_layout.addWidget(self.btnBack)
            btn_layout.addWidget(self.btnFoward)

            self.layout().addLayout(btn_layout)

        self.showNormal()
        self.resize(500, 500)

    def next_image(self):
        if self.index < len(self.images) - 1:
            self.index += 1
        else:
            self.index = 0
        self.load_image()

    def last_image(self):
        if self.index > 0:
            self.index -= 1
        else:
            self.index = len(self.images) - 1
        self.load_image()

    def load_image(self):
        self.viewer.set_photo(QPixmap(self.images[self.index]))

    def pix_info(self):
        self.viewer.toggle_drag_mode()

    def photo_clicked(self, pos):
        if self.viewer.dragMode() == QGraphicsView.NoDrag:
            pass


class PhotoViewer(QGraphicsView):
    photoClicked = Signal(QPoint)

    def __init__(self, parent):
        super(PhotoViewer, self).__init__(parent)
        self._zoom = 0
        self._empty = True
        self._scene = QGraphicsScene(self)
        self._photo = QGraphicsPixmapItem()
        self._scene.addItem(self._photo)
        self.setScene(self._scene)
        self.setTransformationAnchor(QGraphicsView.AnchorUnderMouse)
        self.setResizeAnchor(QGraphicsView.AnchorUnderMouse)
        self.setVerticalScrollBarPolicy(Qt.ScrollBarAlwaysOff)
        self.setHorizontalScrollBarPolicy(Qt.ScrollBarAlwaysOff)
        self.setBackgroundBrush(QBrush(QColor(30, 30, 30)))
        self.setFrameShape(QFrame.NoFrame)

    def has_photo(self):
        return not self._empty

    def fitInView(self, scale=True):
        rect = QRectF(self._photo.pixmap().rect())
        if not rect.isNull():
            self.setSceneRect(rect)
            if self.has_photo():
                unity = self.transform().mapRect(QRectF(0, 0, 1, 1))
                self.scale(1 / unity.width(), 1 / unity.height())
                viewrect = self.viewport().rect()
                scenerect = self.transform().mapRect(rect)
                factor = min(viewrect.width() / scenerect.width(),
                             viewrect.height() / scenerect.height())
                self.scale(factor, factor)
            self._zoom = 0

    def set_photo(self, pixmap=None):
        self._zoom = 0
        if pixmap and not pixmap.isNull():
            self._empty = False
            self.setDragMode(QGraphicsView.ScrollHandDrag)
            self._photo.setPixmap(pixmap)
        else:
            self._empty = True
            self.setDragMode(QGraphicsView.NoDrag)
            self._photo.setPixmap(QPixmap())
        self.fitInView()

    def wheelEvent(self, event):
        if self.has_photo():
            if event.angleDelta().y() > 0:
                factor = 1.05
                self._zoom += 1
            else:
                factor = 0.95
                self._zoom -= 1
            if self._zoom > 0:
                self.scale(factor, factor)
            elif self._zoom == 0:
                self.fitInView()
            else:
                if self._zoom > -10:
                    self.scale(factor, factor)
                else:
                    self._zoom = -10

    def toggle_drag_mode(self):
        if self.dragMode() == QGraphicsView.ScrollHandDrag:
            self.setDragMode(QGraphicsView.NoDrag)
        elif not self._photo.pixmap().isNull():
            self.setDragMode(QGraphicsView.ScrollHandDrag)

    def mousePressEvent(self, event):
        if self._photo.isUnderMouse():
            self.photoClicked.emit(QPoint(event.pos()))
        super(PhotoViewer, self).mousePressEvent(event)


if __name__ == '__main__':
    app = QApplication(sys.argv)
    main = ImageViewer()
    main.load_image()
    main.resize(500, 500)
    main.show()
    sys.exit(app.exec_())
