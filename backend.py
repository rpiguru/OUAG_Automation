import os
import sys
import threading
import time
import picamera
from datetime import datetime

from pymongo import MongoClient

from settings import MONGO_HOST, MONGO_DB, MONGO_COLLECTION, IMAGE_DIR
from utils.ads1115 import ADS1115
from utils.bme280 import BME280
from utils.common import get_config, check_running_proc, repair_mongodb, update_config_file
from utils.logger import logger
from w1thermsensor import W1ThermSensor


ph_acid_volt = 2032.44
ph_neutral_volt = 1500.0


class OUAGBackend(threading.Thread):

    def __init__(self):
        super().__init__()

    def start(self) -> None:
        mongo_client = MongoClient(MONGO_HOST)
        mongo_db = mongo_client[MONGO_DB]
        mongo_col = mongo_db[MONGO_COLLECTION]
        ads = ADS1115()
        bme = BME280()
        w1 = W1ThermSensor()
        while True:
            last_reading_time = get_config().get('last_reading_time', 0)
            if time.time() - last_reading_time > get_config()['interval']:
                w_temp = w1.get_temperature()

                # https://github.com/DFRobot/DFRobot_PH/blob/master/RaspberryPi/Python/DFRobot_PH.py#L22
                ph_v = ads.read_adc(channel=0, gain=1) / 65536 * 4.096 * 2
                slope = (7.0 - 4.0) / ((ph_neutral_volt - 1500.0) / 3.0 - (ph_acid_volt - 1500.0) / 3.0)
                intercept = 7.0 - slope * (ph_neutral_volt - 1500.0) / 3.0
                ph = slope * (ph_v * 1000 - 1500.0) / 3.0 + intercept

                # http://www.cqrobot.wiki/index.php/TDS_Meter_Sensor
                ec_v = ads.read_adc(channel=1, gain=1) / 65536 * 4.096 * 2
                ec_v = ec_v / (1 + .02 * (w_temp - 25))
                ec = (133.42 * ec_v * ec_v * ec_v - 255.86 * ec_v * ec_v + 857.39 * ec_v) * .5
                data = dict(
                    ts=datetime.now(),
                    ph=round(ph, 2),
                    ec=int(ec),
                    temp=round(bme.read_temperature_f(), 1),
                    humidity=round(bme.read_humidity(), 1),
                    water_temp=round(w_temp, 1),
                )
                logger.debug(f"Inserting data - {data}")
                mongo_col.insert_one(data)

                with picamera.PiCamera() as camera:
                    camera.resolution = (640, 480)
                    camera.start_preview()
                    # Camera warm-up time
                    time.sleep(2)
                    camera.capture(os.path.join(IMAGE_DIR, f"{datetime.now().strftime('%m-%d-%Y--%H-%M-%S')}.jpg"))

                update_config_file({'last_reading_time': time.time()})

            time.sleep(1)


if __name__ == '__main__':

    sys._excepthook = sys.excepthook

    def exception_hook(exctype, value, tb):
        logger.error("=========== Backend Crashed!", exc_info=(exctype, value, tb))
        getattr(sys, "_excepthook")(exctype, value, tb)
        sys.exit(1)

    sys.excepthook = exception_hook

    logger.info(f"========== Starting OUAG Backend ==========")

    if not check_running_proc('mongo'):
        logger.warning('MongoDB service is not running... repairing...')
        repair_mongodb()

    b = OUAGBackend()
    b.start()
