# -*- coding: utf-8 -*-

################################################################################
## Form generated from reading UI file 'ouag.ui'
##
## Created by: Qt User Interface Compiler version 5.15.1
##
## WARNING! All changes made in this file will be lost when recompiling UI file!
################################################################################

from PySide2.QtCore import *
from PySide2.QtGui import *
from PySide2.QtWidgets import *

import ui.ouag_rc

class Ui_OUAGWindow(object):
    def setupUi(self, OUAGWindow):
        if not OUAGWindow.objectName():
            OUAGWindow.setObjectName(u"OUAGWindow")
        OUAGWindow.resize(1024, 600)
        sizePolicy = QSizePolicy(QSizePolicy.Fixed, QSizePolicy.Fixed)
        sizePolicy.setHorizontalStretch(0)
        sizePolicy.setVerticalStretch(0)
        sizePolicy.setHeightForWidth(OUAGWindow.sizePolicy().hasHeightForWidth())
        OUAGWindow.setSizePolicy(sizePolicy)
        OUAGWindow.setMinimumSize(QSize(1024, 600))
        OUAGWindow.setMaximumSize(QSize(1024, 600))
        font = QFont()
        font.setPointSize(16)
        OUAGWindow.setFont(font)
        OUAGWindow.setStyleSheet(u"")
        self.centralwidget = QWidget(OUAGWindow)
        self.centralwidget.setObjectName(u"centralwidget")
        self.centralwidget.setStyleSheet(u"#centralwidget {\n"
"background-color: rgb(214, 234, 182);\n"
"}")
        self.verticalLayout_11 = QVBoxLayout(self.centralwidget)
        self.verticalLayout_11.setSpacing(0)
        self.verticalLayout_11.setObjectName(u"verticalLayout_11")
        self.horizontalLayout_3 = QHBoxLayout()
        self.horizontalLayout_3.setObjectName(u"horizontalLayout_3")
        self.label = QLabel(self.centralwidget)
        self.label.setObjectName(u"label")
        self.label.setPixmap(QPixmap(u":/img/img/logo_small.png"))

        self.horizontalLayout_3.addWidget(self.label)

        self.horizontalLayout = QHBoxLayout()
        self.horizontalLayout.setSpacing(0)
        self.horizontalLayout.setObjectName(u"horizontalLayout")
        self.btn_home = QPushButton(self.centralwidget)
        self.btn_home.setObjectName(u"btn_home")
        font1 = QFont()
        font1.setFamily(u"Comic Sans MS")
        font1.setPointSize(12)
        font1.setBold(True)
        font1.setWeight(75)
        self.btn_home.setFont(font1)
        self.btn_home.setStyleSheet(u"background-color: rgb(255, 255, 255);")

        self.horizontalLayout.addWidget(self.btn_home)

        self.btn_shop = QPushButton(self.centralwidget)
        self.btn_shop.setObjectName(u"btn_shop")
        self.btn_shop.setFont(font1)
        self.btn_shop.setStyleSheet(u"background-color: rgb(255, 255, 255);")

        self.horizontalLayout.addWidget(self.btn_shop)

        self.btn_camera = QPushButton(self.centralwidget)
        self.btn_camera.setObjectName(u"btn_camera")
        self.btn_camera.setFont(font1)
        self.btn_camera.setStyleSheet(u"background-color: rgb(255, 255, 255);")

        self.horizontalLayout.addWidget(self.btn_camera)

        self.btn_settings = QPushButton(self.centralwidget)
        self.btn_settings.setObjectName(u"btn_settings")
        self.btn_settings.setFont(font1)
        self.btn_settings.setStyleSheet(u"background-color: rgb(255, 255, 255);")

        self.horizontalLayout.addWidget(self.btn_settings)


        self.horizontalLayout_3.addLayout(self.horizontalLayout)

        self.horizontalSpacer = QSpacerItem(40, 20, QSizePolicy.Expanding, QSizePolicy.Minimum)

        self.horizontalLayout_3.addItem(self.horizontalSpacer)

        self.verticalLayout = QVBoxLayout()
        self.verticalLayout.setObjectName(u"verticalLayout")
        self.horizontalLayout_2 = QHBoxLayout()
        self.horizontalLayout_2.setSpacing(10)
        self.horizontalLayout_2.setObjectName(u"horizontalLayout_2")
        self.btn_system_update = QPushButton(self.centralwidget)
        self.btn_system_update.setObjectName(u"btn_system_update")
        self.btn_system_update.setMinimumSize(QSize(180, 35))
        font2 = QFont()
        font2.setFamily(u"Comic Sans MS")
        font2.setPointSize(12)
        self.btn_system_update.setFont(font2)
        self.btn_system_update.setStyleSheet(u"background-color: rgb(112, 182, 3);\n"
"color: rgb(255, 255, 255);\n"
"border-radius: 5px;\n"
"checked { background-color: red;}")

        self.horizontalLayout_2.addWidget(self.btn_system_update)

        self.btn_software_update = QPushButton(self.centralwidget)
        self.btn_software_update.setObjectName(u"btn_software_update")
        self.btn_software_update.setMinimumSize(QSize(180, 35))
        self.btn_software_update.setFont(font2)
        self.btn_software_update.setStyleSheet(u"background-color: rgb(112, 182, 3);\n"
"color: rgb(255, 255, 255);\n"
"border-radius: 5px;\n"
"checked { background-color: red;}")

        self.horizontalLayout_2.addWidget(self.btn_software_update)


        self.verticalLayout.addLayout(self.horizontalLayout_2)

        self.label_2 = QLabel(self.centralwidget)
        self.label_2.setObjectName(u"label_2")
        font3 = QFont()
        font3.setFamily(u"Comic Sans MS")
        font3.setPointSize(9)
        font3.setBold(False)
        font3.setWeight(50)
        self.label_2.setFont(font3)
        self.label_2.setAlignment(Qt.AlignRight|Qt.AlignTrailing|Qt.AlignVCenter)

        self.verticalLayout.addWidget(self.label_2)


        self.horizontalLayout_3.addLayout(self.verticalLayout)


        self.verticalLayout_11.addLayout(self.horizontalLayout_3)

        self.stack_widget = QStackedWidget(self.centralwidget)
        self.stack_widget.setObjectName(u"stack_widget")
        self.stack_widget.setStyleSheet(u"#stack_widget {\n"
"background-color: rgb(214, 234, 182);\n"
"}")
        self.page_home = QWidget()
        self.page_home.setObjectName(u"page_home")
        self.page_home.setStyleSheet(u"#page_home {\n"
"background-color: rgb(214, 234, 182);\n"
"}")
        self.verticalLayout_2 = QVBoxLayout(self.page_home)
        self.verticalLayout_2.setSpacing(20)
        self.verticalLayout_2.setObjectName(u"verticalLayout_2")
        self.horizontalLayout_14 = QHBoxLayout()
        self.horizontalLayout_14.setSpacing(0)
        self.horizontalLayout_14.setObjectName(u"horizontalLayout_14")
        self.widget = QWidget(self.page_home)
        self.widget.setObjectName(u"widget")
        self.widget.setLayoutDirection(Qt.LeftToRight)
        self.verticalLayout_3 = QVBoxLayout(self.widget)
        self.verticalLayout_3.setSpacing(0)
        self.verticalLayout_3.setObjectName(u"verticalLayout_3")
        self.verticalLayout_3.setContentsMargins(5, 5, 5, 5)
        self.widget_5 = QWidget(self.widget)
        self.widget_5.setObjectName(u"widget_5")
        self.widget_5.setStyleSheet(u"background-color: rgb(255, 255, 255);\n"
"border-radius: 5px;")
        self.verticalLayout_7 = QVBoxLayout(self.widget_5)
        self.verticalLayout_7.setObjectName(u"verticalLayout_7")
        self.verticalLayout_7.setContentsMargins(0, 5, 0, 0)
        self.label_3 = QLabel(self.widget_5)
        self.label_3.setObjectName(u"label_3")
        sizePolicy1 = QSizePolicy(QSizePolicy.Preferred, QSizePolicy.Fixed)
        sizePolicy1.setHorizontalStretch(0)
        sizePolicy1.setVerticalStretch(0)
        sizePolicy1.setHeightForWidth(self.label_3.sizePolicy().hasHeightForWidth())
        self.label_3.setSizePolicy(sizePolicy1)
        font4 = QFont()
        font4.setFamily(u"Comic Sans MS")
        font4.setPointSize(10)
        self.label_3.setFont(font4)
        self.label_3.setStyleSheet(u"color: rgb(127, 127, 127);")
        self.label_3.setAlignment(Qt.AlignCenter)

        self.verticalLayout_7.addWidget(self.label_3)

        self.horizontalLayout_6 = QHBoxLayout()
        self.horizontalLayout_6.setObjectName(u"horizontalLayout_6")
        self.horizontalLayout_6.setContentsMargins(50, -1, -1, -1)
        self.cur_ph = QLabel(self.widget_5)
        self.cur_ph.setObjectName(u"cur_ph")
        sizePolicy2 = QSizePolicy(QSizePolicy.Minimum, QSizePolicy.Fixed)
        sizePolicy2.setHorizontalStretch(0)
        sizePolicy2.setVerticalStretch(0)
        sizePolicy2.setHeightForWidth(self.cur_ph.sizePolicy().hasHeightForWidth())
        self.cur_ph.setSizePolicy(sizePolicy2)
        self.cur_ph.setMaximumSize(QSize(16777215, 45))
        font5 = QFont()
        font5.setFamily(u"Comic Sans MS")
        font5.setPointSize(24)
        self.cur_ph.setFont(font5)
        self.cur_ph.setStyleSheet(u"color: rgb(127, 127, 127);")
        self.cur_ph.setAlignment(Qt.AlignRight|Qt.AlignTrailing|Qt.AlignVCenter)

        self.horizontalLayout_6.addWidget(self.cur_ph)

        self.label_7 = QLabel(self.widget_5)
        self.label_7.setObjectName(u"label_7")
        sizePolicy3 = QSizePolicy(QSizePolicy.Minimum, QSizePolicy.Minimum)
        sizePolicy3.setHorizontalStretch(0)
        sizePolicy3.setVerticalStretch(0)
        sizePolicy3.setHeightForWidth(self.label_7.sizePolicy().hasHeightForWidth())
        self.label_7.setSizePolicy(sizePolicy3)
        self.label_7.setMaximumSize(QSize(80, 16777215))
        self.label_7.setFont(font2)
        self.label_7.setStyleSheet(u"color: rgb(127, 127, 127);")
        self.label_7.setAlignment(Qt.AlignBottom|Qt.AlignLeading|Qt.AlignLeft)

        self.horizontalLayout_6.addWidget(self.label_7)


        self.verticalLayout_7.addLayout(self.horizontalLayout_6)

        self.ts_ph = QLabel(self.widget_5)
        self.ts_ph.setObjectName(u"ts_ph")
        self.ts_ph.setFont(font4)
        self.ts_ph.setStyleSheet(u"color: rgb(127, 127, 127);")
        self.ts_ph.setAlignment(Qt.AlignCenter)

        self.verticalLayout_7.addWidget(self.ts_ph)

        self.widget_10 = QWidget(self.widget_5)
        self.widget_10.setObjectName(u"widget_10")
        sizePolicy4 = QSizePolicy(QSizePolicy.Preferred, QSizePolicy.Expanding)
        sizePolicy4.setHorizontalStretch(0)
        sizePolicy4.setVerticalStretch(0)
        sizePolicy4.setHeightForWidth(self.widget_10.sizePolicy().hasHeightForWidth())
        self.widget_10.setSizePolicy(sizePolicy4)
        self.layout_ph = QVBoxLayout(self.widget_10)
        self.layout_ph.setObjectName(u"layout_ph")
        self.layout_ph.setContentsMargins(17, -1, 17, -1)

        self.verticalLayout_7.addWidget(self.widget_10)


        self.verticalLayout_3.addWidget(self.widget_5)

        self.widget_9 = QWidget(self.widget)
        self.widget_9.setObjectName(u"widget_9")
        sizePolicy1.setHeightForWidth(self.widget_9.sizePolicy().hasHeightForWidth())
        self.widget_9.setSizePolicy(sizePolicy1)
        self.horizontalLayout_7 = QHBoxLayout(self.widget_9)
        self.horizontalLayout_7.setSpacing(0)
        self.horizontalLayout_7.setObjectName(u"horizontalLayout_7")
        self.horizontalLayout_7.setContentsMargins(0, 0, 0, 0)

        self.verticalLayout_3.addWidget(self.widget_9)


        self.horizontalLayout_14.addWidget(self.widget)

        self.widget_2 = QWidget(self.page_home)
        self.widget_2.setObjectName(u"widget_2")
        sizePolicy4.setHeightForWidth(self.widget_2.sizePolicy().hasHeightForWidth())
        self.widget_2.setSizePolicy(sizePolicy4)
        self.verticalLayout_4 = QVBoxLayout(self.widget_2)
        self.verticalLayout_4.setSpacing(0)
        self.verticalLayout_4.setObjectName(u"verticalLayout_4")
        self.verticalLayout_4.setContentsMargins(5, 5, 5, 5)
        self.widget_6 = QWidget(self.widget_2)
        self.widget_6.setObjectName(u"widget_6")
        self.widget_6.setStyleSheet(u"background-color: rgb(255, 255, 255);\n"
"border-radius: 5px;")
        self.verticalLayout_8 = QVBoxLayout(self.widget_6)
        self.verticalLayout_8.setObjectName(u"verticalLayout_8")
        self.verticalLayout_8.setContentsMargins(0, 5, 0, 0)
        self.label_4 = QLabel(self.widget_6)
        self.label_4.setObjectName(u"label_4")
        self.label_4.setFont(font4)
        self.label_4.setStyleSheet(u"color: rgb(127, 127, 127);")
        self.label_4.setAlignment(Qt.AlignCenter)

        self.verticalLayout_8.addWidget(self.label_4)

        self.horizontalLayout_8 = QHBoxLayout()
        self.horizontalLayout_8.setObjectName(u"horizontalLayout_8")
        self.horizontalLayout_8.setContentsMargins(40, -1, -1, -1)
        self.cur_ec = QLabel(self.widget_6)
        self.cur_ec.setObjectName(u"cur_ec")
        sizePolicy2.setHeightForWidth(self.cur_ec.sizePolicy().hasHeightForWidth())
        self.cur_ec.setSizePolicy(sizePolicy2)
        self.cur_ec.setMaximumSize(QSize(16777215, 45))
        self.cur_ec.setFont(font5)
        self.cur_ec.setStyleSheet(u"color: rgb(127, 127, 127);")
        self.cur_ec.setAlignment(Qt.AlignRight|Qt.AlignTrailing|Qt.AlignVCenter)

        self.horizontalLayout_8.addWidget(self.cur_ec)

        self.label_8 = QLabel(self.widget_6)
        self.label_8.setObjectName(u"label_8")
        sizePolicy3.setHeightForWidth(self.label_8.sizePolicy().hasHeightForWidth())
        self.label_8.setSizePolicy(sizePolicy3)
        self.label_8.setMaximumSize(QSize(80, 16777215))
        self.label_8.setFont(font2)
        self.label_8.setStyleSheet(u"color: rgb(127, 127, 127);")
        self.label_8.setAlignment(Qt.AlignBottom|Qt.AlignLeading|Qt.AlignLeft)

        self.horizontalLayout_8.addWidget(self.label_8)


        self.verticalLayout_8.addLayout(self.horizontalLayout_8)

        self.ts_ec = QLabel(self.widget_6)
        self.ts_ec.setObjectName(u"ts_ec")
        self.ts_ec.setFont(font4)
        self.ts_ec.setStyleSheet(u"color: rgb(127, 127, 127);")
        self.ts_ec.setAlignment(Qt.AlignCenter)

        self.verticalLayout_8.addWidget(self.ts_ec)

        self.widget_11 = QWidget(self.widget_6)
        self.widget_11.setObjectName(u"widget_11")
        sizePolicy4.setHeightForWidth(self.widget_11.sizePolicy().hasHeightForWidth())
        self.widget_11.setSizePolicy(sizePolicy4)
        self.layout_ec = QVBoxLayout(self.widget_11)
        self.layout_ec.setObjectName(u"layout_ec")
        self.layout_ec.setContentsMargins(17, -1, 17, -1)

        self.verticalLayout_8.addWidget(self.widget_11)


        self.verticalLayout_4.addWidget(self.widget_6)


        self.horizontalLayout_14.addWidget(self.widget_2)

        self.widget_3 = QWidget(self.page_home)
        self.widget_3.setObjectName(u"widget_3")
        self.verticalLayout_5 = QVBoxLayout(self.widget_3)
        self.verticalLayout_5.setSpacing(0)
        self.verticalLayout_5.setObjectName(u"verticalLayout_5")
        self.verticalLayout_5.setContentsMargins(5, 5, 5, 5)
        self.widget_7 = QWidget(self.widget_3)
        self.widget_7.setObjectName(u"widget_7")
        self.widget_7.setStyleSheet(u"background-color: rgb(255, 255, 255);\n"
"border-radius: 5px;")
        self.verticalLayout_9 = QVBoxLayout(self.widget_7)
        self.verticalLayout_9.setObjectName(u"verticalLayout_9")
        self.verticalLayout_9.setContentsMargins(0, 5, 0, 0)
        self.label_5 = QLabel(self.widget_7)
        self.label_5.setObjectName(u"label_5")
        self.label_5.setFont(font4)
        self.label_5.setStyleSheet(u"color: rgb(127, 127, 127);")
        self.label_5.setAlignment(Qt.AlignCenter)

        self.verticalLayout_9.addWidget(self.label_5)

        self.horizontalLayout_9 = QHBoxLayout()
        self.horizontalLayout_9.setObjectName(u"horizontalLayout_9")
        self.horizontalLayout_9.setContentsMargins(50, -1, -1, -1)
        self.cur_temp = QLabel(self.widget_7)
        self.cur_temp.setObjectName(u"cur_temp")
        sizePolicy2.setHeightForWidth(self.cur_temp.sizePolicy().hasHeightForWidth())
        self.cur_temp.setSizePolicy(sizePolicy2)
        self.cur_temp.setMaximumSize(QSize(16777215, 45))
        self.cur_temp.setFont(font5)
        self.cur_temp.setStyleSheet(u"color: rgb(127, 127, 127);")
        self.cur_temp.setAlignment(Qt.AlignRight|Qt.AlignTrailing|Qt.AlignVCenter)

        self.horizontalLayout_9.addWidget(self.cur_temp)

        self.label_9 = QLabel(self.widget_7)
        self.label_9.setObjectName(u"label_9")
        sizePolicy3.setHeightForWidth(self.label_9.sizePolicy().hasHeightForWidth())
        self.label_9.setSizePolicy(sizePolicy3)
        self.label_9.setMaximumSize(QSize(80, 16777215))
        self.label_9.setFont(font2)
        self.label_9.setStyleSheet(u"color: rgb(127, 127, 127);")
        self.label_9.setAlignment(Qt.AlignBottom|Qt.AlignLeading|Qt.AlignLeft)

        self.horizontalLayout_9.addWidget(self.label_9)


        self.verticalLayout_9.addLayout(self.horizontalLayout_9)

        self.ts_temp = QLabel(self.widget_7)
        self.ts_temp.setObjectName(u"ts_temp")
        self.ts_temp.setFont(font4)
        self.ts_temp.setStyleSheet(u"color: rgb(127, 127, 127);")
        self.ts_temp.setAlignment(Qt.AlignCenter)

        self.verticalLayout_9.addWidget(self.ts_temp)

        self.widget_12 = QWidget(self.widget_7)
        self.widget_12.setObjectName(u"widget_12")
        sizePolicy4.setHeightForWidth(self.widget_12.sizePolicy().hasHeightForWidth())
        self.widget_12.setSizePolicy(sizePolicy4)
        self.layout_temp = QVBoxLayout(self.widget_12)
        self.layout_temp.setObjectName(u"layout_temp")
        self.layout_temp.setContentsMargins(17, -1, 17, -1)

        self.verticalLayout_9.addWidget(self.widget_12)


        self.verticalLayout_5.addWidget(self.widget_7)


        self.horizontalLayout_14.addWidget(self.widget_3)

        self.widget_14 = QWidget(self.page_home)
        self.widget_14.setObjectName(u"widget_14")
        self.verticalLayout_15 = QVBoxLayout(self.widget_14)
        self.verticalLayout_15.setSpacing(0)
        self.verticalLayout_15.setObjectName(u"verticalLayout_15")
        self.verticalLayout_15.setContentsMargins(5, 5, 5, 5)
        self.widget_15 = QWidget(self.widget_14)
        self.widget_15.setObjectName(u"widget_15")
        self.widget_15.setStyleSheet(u"background-color: rgb(255, 255, 255);\n"
"border-radius: 5px;")
        self.verticalLayout_16 = QVBoxLayout(self.widget_15)
        self.verticalLayout_16.setObjectName(u"verticalLayout_16")
        self.verticalLayout_16.setContentsMargins(0, 5, 0, 0)
        self.label_14 = QLabel(self.widget_15)
        self.label_14.setObjectName(u"label_14")
        self.label_14.setFont(font4)
        self.label_14.setStyleSheet(u"color: rgb(127, 127, 127);")
        self.label_14.setAlignment(Qt.AlignCenter)

        self.verticalLayout_16.addWidget(self.label_14)

        self.horizontalLayout_13 = QHBoxLayout()
        self.horizontalLayout_13.setObjectName(u"horizontalLayout_13")
        self.horizontalLayout_13.setContentsMargins(50, -1, -1, -1)
        self.cur_water_temp = QLabel(self.widget_15)
        self.cur_water_temp.setObjectName(u"cur_water_temp")
        sizePolicy2.setHeightForWidth(self.cur_water_temp.sizePolicy().hasHeightForWidth())
        self.cur_water_temp.setSizePolicy(sizePolicy2)
        self.cur_water_temp.setMaximumSize(QSize(16777215, 45))
        self.cur_water_temp.setFont(font5)
        self.cur_water_temp.setStyleSheet(u"color: rgb(127, 127, 127);")
        self.cur_water_temp.setAlignment(Qt.AlignRight|Qt.AlignTrailing|Qt.AlignVCenter)

        self.horizontalLayout_13.addWidget(self.cur_water_temp)

        self.label_15 = QLabel(self.widget_15)
        self.label_15.setObjectName(u"label_15")
        sizePolicy3.setHeightForWidth(self.label_15.sizePolicy().hasHeightForWidth())
        self.label_15.setSizePolicy(sizePolicy3)
        self.label_15.setMaximumSize(QSize(80, 16777215))
        self.label_15.setFont(font2)
        self.label_15.setStyleSheet(u"color: rgb(127, 127, 127);")
        self.label_15.setAlignment(Qt.AlignBottom|Qt.AlignLeading|Qt.AlignLeft)

        self.horizontalLayout_13.addWidget(self.label_15)


        self.verticalLayout_16.addLayout(self.horizontalLayout_13)

        self.ts_water_temp = QLabel(self.widget_15)
        self.ts_water_temp.setObjectName(u"ts_water_temp")
        self.ts_water_temp.setFont(font4)
        self.ts_water_temp.setStyleSheet(u"color: rgb(127, 127, 127);")
        self.ts_water_temp.setAlignment(Qt.AlignCenter)

        self.verticalLayout_16.addWidget(self.ts_water_temp)

        self.widget_16 = QWidget(self.widget_15)
        self.widget_16.setObjectName(u"widget_16")
        sizePolicy4.setHeightForWidth(self.widget_16.sizePolicy().hasHeightForWidth())
        self.widget_16.setSizePolicy(sizePolicy4)
        self.layout_water_temp = QVBoxLayout(self.widget_16)
        self.layout_water_temp.setObjectName(u"layout_water_temp")
        self.layout_water_temp.setContentsMargins(17, -1, 17, -1)

        self.verticalLayout_16.addWidget(self.widget_16)


        self.verticalLayout_15.addWidget(self.widget_15)


        self.horizontalLayout_14.addWidget(self.widget_14)

        self.widget_4 = QWidget(self.page_home)
        self.widget_4.setObjectName(u"widget_4")
        self.verticalLayout_6 = QVBoxLayout(self.widget_4)
        self.verticalLayout_6.setSpacing(0)
        self.verticalLayout_6.setObjectName(u"verticalLayout_6")
        self.verticalLayout_6.setContentsMargins(5, 5, 5, 5)
        self.widget_8 = QWidget(self.widget_4)
        self.widget_8.setObjectName(u"widget_8")
        self.widget_8.setStyleSheet(u"background-color: rgb(255, 255, 255);\n"
"border-radius: 5px;")
        self.verticalLayout_10 = QVBoxLayout(self.widget_8)
        self.verticalLayout_10.setObjectName(u"verticalLayout_10")
        self.verticalLayout_10.setContentsMargins(0, 5, 0, 0)
        self.label_6 = QLabel(self.widget_8)
        self.label_6.setObjectName(u"label_6")
        self.label_6.setFont(font4)
        self.label_6.setStyleSheet(u"color: rgb(127, 127, 127);")
        self.label_6.setAlignment(Qt.AlignCenter)

        self.verticalLayout_10.addWidget(self.label_6)

        self.horizontalLayout_10 = QHBoxLayout()
        self.horizontalLayout_10.setObjectName(u"horizontalLayout_10")
        self.horizontalLayout_10.setContentsMargins(50, -1, -1, -1)
        self.cur_humidity = QLabel(self.widget_8)
        self.cur_humidity.setObjectName(u"cur_humidity")
        sizePolicy2.setHeightForWidth(self.cur_humidity.sizePolicy().hasHeightForWidth())
        self.cur_humidity.setSizePolicy(sizePolicy2)
        self.cur_humidity.setMaximumSize(QSize(16777215, 45))
        self.cur_humidity.setFont(font5)
        self.cur_humidity.setStyleSheet(u"color: rgb(127, 127, 127);")
        self.cur_humidity.setAlignment(Qt.AlignRight|Qt.AlignTrailing|Qt.AlignVCenter)

        self.horizontalLayout_10.addWidget(self.cur_humidity)

        self.label_10 = QLabel(self.widget_8)
        self.label_10.setObjectName(u"label_10")
        sizePolicy3.setHeightForWidth(self.label_10.sizePolicy().hasHeightForWidth())
        self.label_10.setSizePolicy(sizePolicy3)
        self.label_10.setMaximumSize(QSize(80, 16777215))
        self.label_10.setFont(font2)
        self.label_10.setStyleSheet(u"color: rgb(127, 127, 127);")
        self.label_10.setAlignment(Qt.AlignBottom|Qt.AlignLeading|Qt.AlignLeft)

        self.horizontalLayout_10.addWidget(self.label_10)


        self.verticalLayout_10.addLayout(self.horizontalLayout_10)

        self.ts_humidity = QLabel(self.widget_8)
        self.ts_humidity.setObjectName(u"ts_humidity")
        self.ts_humidity.setFont(font4)
        self.ts_humidity.setStyleSheet(u"color: rgb(127, 127, 127);")
        self.ts_humidity.setAlignment(Qt.AlignCenter)

        self.verticalLayout_10.addWidget(self.ts_humidity)

        self.widget_13 = QWidget(self.widget_8)
        self.widget_13.setObjectName(u"widget_13")
        sizePolicy4.setHeightForWidth(self.widget_13.sizePolicy().hasHeightForWidth())
        self.widget_13.setSizePolicy(sizePolicy4)
        self.layout_humidity = QVBoxLayout(self.widget_13)
        self.layout_humidity.setObjectName(u"layout_humidity")
        self.layout_humidity.setContentsMargins(17, -1, 17, -1)

        self.verticalLayout_10.addWidget(self.widget_13)


        self.verticalLayout_6.addWidget(self.widget_8)


        self.horizontalLayout_14.addWidget(self.widget_4)


        self.verticalLayout_2.addLayout(self.horizontalLayout_14)

        self.horizontalLayout_4 = QHBoxLayout()
        self.horizontalLayout_4.setObjectName(u"horizontalLayout_4")
        self.btn_light_control = QPushButton(self.page_home)
        self.btn_light_control.setObjectName(u"btn_light_control")
        self.btn_light_control.setMinimumSize(QSize(0, 40))
        self.btn_light_control.setMaximumSize(QSize(200, 16777215))
        font6 = QFont()
        font6.setFamily(u"Comic Sans MS")
        font6.setPointSize(14)
        self.btn_light_control.setFont(font6)
        self.btn_light_control.setStyleSheet(u"background-color: rgb(112, 182, 3);\n"
"color: rgb(255, 255, 255);\n"
"border-radius: 5px;\n"
"checked { background-color: red;}")

        self.horizontalLayout_4.addWidget(self.btn_light_control)

        self.btn_exhaust_control = QPushButton(self.page_home)
        self.btn_exhaust_control.setObjectName(u"btn_exhaust_control")
        self.btn_exhaust_control.setMinimumSize(QSize(0, 40))
        self.btn_exhaust_control.setMaximumSize(QSize(200, 16777215))
        self.btn_exhaust_control.setFont(font6)
        self.btn_exhaust_control.setStyleSheet(u"background-color: rgb(112, 182, 3);\n"
"color: rgb(255, 255, 255);\n"
"border-radius: 5px;")

        self.horizontalLayout_4.addWidget(self.btn_exhaust_control)

        self.btn_humidity_control = QPushButton(self.page_home)
        self.btn_humidity_control.setObjectName(u"btn_humidity_control")
        self.btn_humidity_control.setMinimumSize(QSize(0, 40))
        self.btn_humidity_control.setMaximumSize(QSize(200, 16777215))
        self.btn_humidity_control.setFont(font6)
        self.btn_humidity_control.setStyleSheet(u"background-color: rgb(112, 182, 3);\n"
"color: rgb(255, 255, 255);\n"
"border-radius: 5px;")

        self.horizontalLayout_4.addWidget(self.btn_humidity_control)

        self.btn_air_fan_control = QPushButton(self.page_home)
        self.btn_air_fan_control.setObjectName(u"btn_air_fan_control")
        self.btn_air_fan_control.setMinimumSize(QSize(0, 40))
        self.btn_air_fan_control.setMaximumSize(QSize(200, 16777215))
        self.btn_air_fan_control.setFont(font6)
        self.btn_air_fan_control.setStyleSheet(u"background-color: rgb(112, 182, 3);\n"
"color: rgb(255, 255, 255);\n"
"border-radius:5px;")

        self.horizontalLayout_4.addWidget(self.btn_air_fan_control)


        self.verticalLayout_2.addLayout(self.horizontalLayout_4)

        self.horizontalLayout_5 = QHBoxLayout()
        self.horizontalLayout_5.setSpacing(30)
        self.horizontalLayout_5.setObjectName(u"horizontalLayout_5")
        self.horizontalLayout_5.setContentsMargins(-1, 0, -1, -1)
        self.btn_chart = QToolButton(self.page_home)
        self.btn_chart.setObjectName(u"btn_chart")
        self.btn_chart.setMinimumSize(QSize(50, 50))
        self.btn_chart.setMaximumSize(QSize(50, 50))
        icon = QIcon()
        icon.addFile(u":/img/img/3d bar chart.png", QSize(), QIcon.Normal, QIcon.Off)
        self.btn_chart.setIcon(icon)
        self.btn_chart.setIconSize(QSize(50, 50))

        self.horizontalLayout_5.addWidget(self.btn_chart)

        self.btn_refresh = QToolButton(self.page_home)
        self.btn_refresh.setObjectName(u"btn_refresh")
        self.btn_refresh.setMinimumSize(QSize(50, 50))
        icon1 = QIcon()
        icon1.addFile(u":/img/img/Refresh.png", QSize(), QIcon.Normal, QIcon.Off)
        self.btn_refresh.setIcon(icon1)
        self.btn_refresh.setIconSize(QSize(40, 40))

        self.horizontalLayout_5.addWidget(self.btn_refresh)

        self.horizontalSpacer_2 = QSpacerItem(40, 20, QSizePolicy.Expanding, QSizePolicy.Minimum)

        self.horizontalLayout_5.addItem(self.horizontalSpacer_2)

        self.btn_vegetation_stage = QPushButton(self.page_home)
        self.btn_vegetation_stage.setObjectName(u"btn_vegetation_stage")
        self.btn_vegetation_stage.setMinimumSize(QSize(180, 40))
        self.btn_vegetation_stage.setFont(font6)
        self.btn_vegetation_stage.setStyleSheet(u"background-color: rgb(112, 182, 3);\n"
"color: rgb(255, 255, 255);\n"
"border-radius: 5px;")

        self.horizontalLayout_5.addWidget(self.btn_vegetation_stage)

        self.btn_bloom_stage = QPushButton(self.page_home)
        self.btn_bloom_stage.setObjectName(u"btn_bloom_stage")
        self.btn_bloom_stage.setMinimumSize(QSize(180, 40))
        self.btn_bloom_stage.setFont(font6)
        self.btn_bloom_stage.setStyleSheet(u"background-color: rgb(112, 182, 3);\n"
"color: rgb(255, 255, 255);\n"
"border-radius: 5px;")

        self.horizontalLayout_5.addWidget(self.btn_bloom_stage)

        self.horizontalSpacer_3 = QSpacerItem(40, 20, QSizePolicy.Expanding, QSizePolicy.Minimum)

        self.horizontalLayout_5.addItem(self.horizontalSpacer_3)


        self.verticalLayout_2.addLayout(self.horizontalLayout_5)

        self.stack_widget.addWidget(self.page_home)
        self.page_chart = QWidget()
        self.page_chart.setObjectName(u"page_chart")
        self.verticalLayout_12 = QVBoxLayout(self.page_chart)
        self.verticalLayout_12.setObjectName(u"verticalLayout_12")
        self.verticalLayout_12.setContentsMargins(20, 20, 20, 20)
        self.horizontalLayout_12 = QHBoxLayout()
        self.horizontalLayout_12.setSpacing(10)
        self.horizontalLayout_12.setObjectName(u"horizontalLayout_12")
        self.horizontalSpacer_5 = QSpacerItem(40, 20, QSizePolicy.Expanding, QSizePolicy.Minimum)

        self.horizontalLayout_12.addItem(self.horizontalSpacer_5)

        self.label_11 = QLabel(self.page_chart)
        self.label_11.setObjectName(u"label_11")
        self.label_11.setFont(font2)

        self.horizontalLayout_12.addWidget(self.label_11)

        self.combo_sensor = QComboBox(self.page_chart)
        self.combo_sensor.addItem("")
        self.combo_sensor.addItem("")
        self.combo_sensor.addItem("")
        self.combo_sensor.addItem("")
        self.combo_sensor.addItem("")
        self.combo_sensor.setObjectName(u"combo_sensor")
        self.combo_sensor.setFont(font2)

        self.horizontalLayout_12.addWidget(self.combo_sensor)

        self.horizontalSpacer_4 = QSpacerItem(40, 20, QSizePolicy.Expanding, QSizePolicy.Minimum)

        self.horizontalLayout_12.addItem(self.horizontalSpacer_4)


        self.verticalLayout_12.addLayout(self.horizontalLayout_12)

        self.frame = QFrame(self.page_chart)
        self.frame.setObjectName(u"frame")
        sizePolicy4.setHeightForWidth(self.frame.sizePolicy().hasHeightForWidth())
        self.frame.setSizePolicy(sizePolicy4)
        self.frame.setFrameShape(QFrame.StyledPanel)
        self.frame.setFrameShadow(QFrame.Raised)
        self.layout_chart = QVBoxLayout(self.frame)
        self.layout_chart.setObjectName(u"layout_chart")
        self.layout_chart.setContentsMargins(0, 0, 0, 0)

        self.verticalLayout_12.addWidget(self.frame)

        self.stack_widget.addWidget(self.page_chart)
        self.page_camera = QWidget()
        self.page_camera.setObjectName(u"page_camera")
        self.page_camera.setStyleSheet(u"#page_camera {\n"
"background-color: rgb(214, 234, 182);\n"
"}")
        self.horizontalLayout_11 = QHBoxLayout(self.page_camera)
        self.horizontalLayout_11.setObjectName(u"horizontalLayout_11")
        self.image_frame = QFrame(self.page_camera)
        self.image_frame.setObjectName(u"image_frame")
        sizePolicy5 = QSizePolicy(QSizePolicy.Expanding, QSizePolicy.Preferred)
        sizePolicy5.setHorizontalStretch(0)
        sizePolicy5.setVerticalStretch(0)
        sizePolicy5.setHeightForWidth(self.image_frame.sizePolicy().hasHeightForWidth())
        self.image_frame.setSizePolicy(sizePolicy5)
        self.image_frame.setMinimumSize(QSize(820, 482))
        self.image_frame.setFrameShape(QFrame.StyledPanel)
        self.image_frame.setFrameShadow(QFrame.Raised)
        self.layoutImage = QVBoxLayout(self.image_frame)
        self.layoutImage.setObjectName(u"layoutImage")

        self.horizontalLayout_11.addWidget(self.image_frame)

        self.verticalLayout_14 = QVBoxLayout()
        self.verticalLayout_14.setSpacing(30)
        self.verticalLayout_14.setObjectName(u"verticalLayout_14")
        self.verticalLayout_14.setContentsMargins(-1, 30, -1, 30)
        self.verticalLayout_13 = QVBoxLayout()
        self.verticalLayout_13.setObjectName(u"verticalLayout_13")
        self.label_13 = QLabel(self.page_camera)
        self.label_13.setObjectName(u"label_13")
        font7 = QFont()
        font7.setFamily(u"Comic Sans MS")
        font7.setPointSize(10)
        font7.setItalic(True)
        self.label_13.setFont(font7)

        self.verticalLayout_13.addWidget(self.label_13)

        self.snap_interval = QComboBox(self.page_camera)
        self.snap_interval.addItem("")
        self.snap_interval.addItem("")
        self.snap_interval.addItem("")
        self.snap_interval.addItem("")
        self.snap_interval.addItem("")
        self.snap_interval.addItem("")
        self.snap_interval.addItem("")
        self.snap_interval.addItem("")
        self.snap_interval.addItem("")
        self.snap_interval.setObjectName(u"snap_interval")
        self.snap_interval.setMinimumSize(QSize(0, 30))
        font8 = QFont()
        font8.setPointSize(12)
        self.snap_interval.setFont(font8)

        self.verticalLayout_13.addWidget(self.snap_interval)


        self.verticalLayout_14.addLayout(self.verticalLayout_13)

        self.verticalSpacer = QSpacerItem(20, 40, QSizePolicy.Minimum, QSizePolicy.Expanding)

        self.verticalLayout_14.addItem(self.verticalSpacer)

        self.btn_take_photo = QPushButton(self.page_camera)
        self.btn_take_photo.setObjectName(u"btn_take_photo")
        sizePolicy1.setHeightForWidth(self.btn_take_photo.sizePolicy().hasHeightForWidth())
        self.btn_take_photo.setSizePolicy(sizePolicy1)
        self.btn_take_photo.setMinimumSize(QSize(160, 35))
        self.btn_take_photo.setFont(font2)
        self.btn_take_photo.setStyleSheet(u"background-color: rgb(112, 182, 3);\n"
"color: rgb(255, 255, 255);\n"
"border-radius: 5px;\n"
"checked { background-color: red;}")
        icon2 = QIcon()
        icon2.addFile(u":/img/img/Camera.png", QSize(), QIcon.Normal, QIcon.Off)
        self.btn_take_photo.setIcon(icon2)
        self.btn_take_photo.setIconSize(QSize(30, 30))

        self.verticalLayout_14.addWidget(self.btn_take_photo)

        self.btn_photo_archive = QPushButton(self.page_camera)
        self.btn_photo_archive.setObjectName(u"btn_photo_archive")
        sizePolicy1.setHeightForWidth(self.btn_photo_archive.sizePolicy().hasHeightForWidth())
        self.btn_photo_archive.setSizePolicy(sizePolicy1)
        self.btn_photo_archive.setMinimumSize(QSize(160, 35))
        self.btn_photo_archive.setFont(font2)
        self.btn_photo_archive.setStyleSheet(u"background-color: rgb(112, 182, 3);\n"
"color: rgb(255, 255, 255);\n"
"border-radius: 5px;\n"
"checked { background-color: red;}")
        icon3 = QIcon()
        icon3.addFile(u":/img/img/Report.png", QSize(), QIcon.Normal, QIcon.Off)
        self.btn_photo_archive.setIcon(icon3)
        self.btn_photo_archive.setIconSize(QSize(30, 30))

        self.verticalLayout_14.addWidget(self.btn_photo_archive)


        self.horizontalLayout_11.addLayout(self.verticalLayout_14)

        self.stack_widget.addWidget(self.page_camera)
        self.page_settings = QWidget()
        self.page_settings.setObjectName(u"page_settings")
        self.page_settings.setStyleSheet(u"#page_settings {\n"
"	background-color: rgb(214, 234, 182);\n"
"}")
        self.verticalLayout_17 = QVBoxLayout(self.page_settings)
        self.verticalLayout_17.setObjectName(u"verticalLayout_17")
        self.tab_widget = QTabWidget(self.page_settings)
        self.tab_widget.setObjectName(u"tab_widget")
        font9 = QFont()
        font9.setFamily(u"Comic Sans MS")
        font9.setPointSize(11)
        font9.setItalic(True)
        self.tab_widget.setFont(font9)
        self.tab_widget.setStyleSheet(u"")
        self.tab_wifi = QWidget()
        self.tab_wifi.setObjectName(u"tab_wifi")
        self.tab_wifi.setFont(font2)
        self.tab_wifi.setStyleSheet(u"#tab_wifi {\n"
"	background-color: rgb(214, 234, 182);\n"
"}")
        self.verticalLayout_18 = QVBoxLayout(self.tab_wifi)
        self.verticalLayout_18.setSpacing(30)
        self.verticalLayout_18.setObjectName(u"verticalLayout_18")
        self.verticalLayout_18.setContentsMargins(250, 50, 250, 150)
        self.horizontalLayout_15 = QHBoxLayout()
        self.horizontalLayout_15.setSpacing(20)
        self.horizontalLayout_15.setObjectName(u"horizontalLayout_15")
        self.label_12 = QLabel(self.tab_wifi)
        self.label_12.setObjectName(u"label_12")
        self.label_12.setMinimumSize(QSize(100, 0))
        self.label_12.setFont(font1)

        self.horizontalLayout_15.addWidget(self.label_12)

        self.ssid = QComboBox(self.tab_wifi)
        self.ssid.setObjectName(u"ssid")
        sizePolicy6 = QSizePolicy(QSizePolicy.MinimumExpanding, QSizePolicy.Fixed)
        sizePolicy6.setHorizontalStretch(0)
        sizePolicy6.setVerticalStretch(0)
        sizePolicy6.setHeightForWidth(self.ssid.sizePolicy().hasHeightForWidth())
        self.ssid.setSizePolicy(sizePolicy6)
        self.ssid.setMinimumSize(QSize(0, 30))
        self.ssid.setFont(font2)

        self.horizontalLayout_15.addWidget(self.ssid)

        self.btn_refresh_ssid = QToolButton(self.tab_wifi)
        self.btn_refresh_ssid.setObjectName(u"btn_refresh_ssid")
        self.btn_refresh_ssid.setMinimumSize(QSize(30, 30))
        self.btn_refresh_ssid.setMaximumSize(QSize(40, 40))
        self.btn_refresh_ssid.setIcon(icon1)
        self.btn_refresh_ssid.setIconSize(QSize(30, 30))

        self.horizontalLayout_15.addWidget(self.btn_refresh_ssid)


        self.verticalLayout_18.addLayout(self.horizontalLayout_15)

        self.horizontalLayout_16 = QHBoxLayout()
        self.horizontalLayout_16.setSpacing(20)
        self.horizontalLayout_16.setObjectName(u"horizontalLayout_16")
        self.label_16 = QLabel(self.tab_wifi)
        self.label_16.setObjectName(u"label_16")
        self.label_16.setMinimumSize(QSize(100, 0))
        self.label_16.setFont(font1)

        self.horizontalLayout_16.addWidget(self.label_16)

        self.password = QLineEdit(self.tab_wifi)
        self.password.setObjectName(u"password")
        self.password.setMinimumSize(QSize(0, 30))
        self.password.setFont(font2)
        self.password.setEchoMode(QLineEdit.Password)

        self.horizontalLayout_16.addWidget(self.password)

        self.btn_input_password = QToolButton(self.tab_wifi)
        self.btn_input_password.setObjectName(u"btn_input_password")
        self.btn_input_password.setMinimumSize(QSize(30, 30))
        self.btn_input_password.setMaximumSize(QSize(40, 40))
        icon4 = QIcon()
        icon4.addFile(u":/img/img/Modify.png", QSize(), QIcon.Normal, QIcon.Off)
        self.btn_input_password.setIcon(icon4)
        self.btn_input_password.setIconSize(QSize(30, 30))

        self.horizontalLayout_16.addWidget(self.btn_input_password)


        self.verticalLayout_18.addLayout(self.horizontalLayout_16)

        self.verticalSpacer_2 = QSpacerItem(20, 40, QSizePolicy.Minimum, QSizePolicy.Expanding)

        self.verticalLayout_18.addItem(self.verticalSpacer_2)

        self.btn_connect_wifi = QPushButton(self.tab_wifi)
        self.btn_connect_wifi.setObjectName(u"btn_connect_wifi")
        self.btn_connect_wifi.setMinimumSize(QSize(0, 40))
        self.btn_connect_wifi.setFont(font1)

        self.verticalLayout_18.addWidget(self.btn_connect_wifi)

        self.tab_widget.addTab(self.tab_wifi, "")
        self.tab_calibration = QWidget()
        self.tab_calibration.setObjectName(u"tab_calibration")
        self.tab_widget.addTab(self.tab_calibration, "")
        self.tab_dispense = QWidget()
        self.tab_dispense.setObjectName(u"tab_dispense")
        self.tab_widget.addTab(self.tab_dispense, "")
        self.tab_smart_plugs = QWidget()
        self.tab_smart_plugs.setObjectName(u"tab_smart_plugs")
        self.tab_widget.addTab(self.tab_smart_plugs, "")

        self.verticalLayout_17.addWidget(self.tab_widget)

        self.stack_widget.addWidget(self.page_settings)

        self.verticalLayout_11.addWidget(self.stack_widget)

        OUAGWindow.setCentralWidget(self.centralwidget)
#if QT_CONFIG(shortcut)
        self.label_13.setBuddy(self.snap_interval)
#endif // QT_CONFIG(shortcut)

        self.retranslateUi(OUAGWindow)

        self.stack_widget.setCurrentIndex(0)
        self.tab_widget.setCurrentIndex(0)


        QMetaObject.connectSlotsByName(OUAGWindow)
    # setupUi

    def retranslateUi(self, OUAGWindow):
        OUAGWindow.setWindowTitle(QCoreApplication.translate("OUAGWindow", u"OUAG Automation", None))
        self.label.setText("")
        self.btn_home.setText(QCoreApplication.translate("OUAGWindow", u"Home", None))
        self.btn_shop.setText(QCoreApplication.translate("OUAGWindow", u"Shop", None))
        self.btn_camera.setText(QCoreApplication.translate("OUAGWindow", u"Camera", None))
        self.btn_settings.setText(QCoreApplication.translate("OUAGWindow", u"Settings", None))
        self.btn_system_update.setText(QCoreApplication.translate("OUAGWindow", u"System Update", None))
        self.btn_software_update.setText(QCoreApplication.translate("OUAGWindow", u"Software Update", None))
        self.label_2.setText(QCoreApplication.translate("OUAGWindow", u"Phone Support: (480) 681-5544     ", None))
        self.label_3.setText(QCoreApplication.translate("OUAGWindow", u"Water pH", None))
        self.cur_ph.setText(QCoreApplication.translate("OUAGWindow", u"5.63", None))
        self.label_7.setText(QCoreApplication.translate("OUAGWindow", u"pH", None))
        self.ts_ph.setText(QCoreApplication.translate("OUAGWindow", u"19/01/22 13:23:45", None))
        self.label_4.setText(QCoreApplication.translate("OUAGWindow", u"Water EC", None))
        self.cur_ec.setText(QCoreApplication.translate("OUAGWindow", u"1056", None))
        self.label_8.setText(QCoreApplication.translate("OUAGWindow", u"ppm", None))
        self.ts_ec.setText(QCoreApplication.translate("OUAGWindow", u"19/01/22 13:23:45", None))
        self.label_5.setText(QCoreApplication.translate("OUAGWindow", u"Air Temperature", None))
        self.cur_temp.setText(QCoreApplication.translate("OUAGWindow", u"24.1", None))
        self.label_9.setText(QCoreApplication.translate("OUAGWindow", u"\u00b0F", None))
        self.ts_temp.setText(QCoreApplication.translate("OUAGWindow", u"19/01/22 13:23:45", None))
        self.label_14.setText(QCoreApplication.translate("OUAGWindow", u"Water Temperature", None))
        self.cur_water_temp.setText(QCoreApplication.translate("OUAGWindow", u"24.1", None))
        self.label_15.setText(QCoreApplication.translate("OUAGWindow", u"\u00b0F", None))
        self.ts_water_temp.setText(QCoreApplication.translate("OUAGWindow", u"19/01/22 13:23:45", None))
        self.label_6.setText(QCoreApplication.translate("OUAGWindow", u"Air Humidity", None))
        self.cur_humidity.setText(QCoreApplication.translate("OUAGWindow", u"61.2", None))
        self.label_10.setText(QCoreApplication.translate("OUAGWindow", u"%", None))
        self.ts_humidity.setText(QCoreApplication.translate("OUAGWindow", u"19/01/22 13:23:45", None))
        self.btn_light_control.setText(QCoreApplication.translate("OUAGWindow", u"Light Control", None))
        self.btn_exhaust_control.setText(QCoreApplication.translate("OUAGWindow", u"Exhaust Control", None))
        self.btn_humidity_control.setText(QCoreApplication.translate("OUAGWindow", u"Humidity Control", None))
        self.btn_air_fan_control.setText(QCoreApplication.translate("OUAGWindow", u"Air Fan Control", None))
        self.btn_chart.setText("")
        self.btn_refresh.setText("")
        self.btn_vegetation_stage.setText(QCoreApplication.translate("OUAGWindow", u"Vegetation Stage", None))
        self.btn_bloom_stage.setText(QCoreApplication.translate("OUAGWindow", u"Bloom Stage", None))
        self.label_11.setText(QCoreApplication.translate("OUAGWindow", u"Select Sensor:", None))
        self.combo_sensor.setItemText(0, QCoreApplication.translate("OUAGWindow", u"pH", None))
        self.combo_sensor.setItemText(1, QCoreApplication.translate("OUAGWindow", u"EC", None))
        self.combo_sensor.setItemText(2, QCoreApplication.translate("OUAGWindow", u"Air Temperature", None))
        self.combo_sensor.setItemText(3, QCoreApplication.translate("OUAGWindow", u"Water Temperature", None))
        self.combo_sensor.setItemText(4, QCoreApplication.translate("OUAGWindow", u"Humidity", None))

        self.label_13.setText(QCoreApplication.translate("OUAGWindow", u"Snap Inerval:", None))
        self.snap_interval.setItemText(0, QCoreApplication.translate("OUAGWindow", u"5 min", None))
        self.snap_interval.setItemText(1, QCoreApplication.translate("OUAGWindow", u"15 min", None))
        self.snap_interval.setItemText(2, QCoreApplication.translate("OUAGWindow", u"30 min", None))
        self.snap_interval.setItemText(3, QCoreApplication.translate("OUAGWindow", u"1 hour", None))
        self.snap_interval.setItemText(4, QCoreApplication.translate("OUAGWindow", u"4 hours", None))
        self.snap_interval.setItemText(5, QCoreApplication.translate("OUAGWindow", u"8 hours", None))
        self.snap_interval.setItemText(6, QCoreApplication.translate("OUAGWindow", u"16 hours", None))
        self.snap_interval.setItemText(7, QCoreApplication.translate("OUAGWindow", u"1 day", None))
        self.snap_interval.setItemText(8, QCoreApplication.translate("OUAGWindow", u"None", None))

        self.snap_interval.setCurrentText(QCoreApplication.translate("OUAGWindow", u"5 min", None))
        self.btn_take_photo.setText(QCoreApplication.translate("OUAGWindow", u" Take Photo", None))
        self.btn_photo_archive.setText(QCoreApplication.translate("OUAGWindow", u" Photo Archive", None))
        self.label_12.setText(QCoreApplication.translate("OUAGWindow", u"SSID", None))
        self.btn_refresh_ssid.setText("")
        self.label_16.setText(QCoreApplication.translate("OUAGWindow", u"Password", None))
        self.btn_input_password.setText("")
        self.btn_connect_wifi.setText(QCoreApplication.translate("OUAGWindow", u"Connect", None))
        self.tab_widget.setTabText(self.tab_widget.indexOf(self.tab_wifi), QCoreApplication.translate("OUAGWindow", u"WiFi Settings", None))
        self.tab_widget.setTabText(self.tab_widget.indexOf(self.tab_calibration), QCoreApplication.translate("OUAGWindow", u"Sensor Calilbration", None))
        self.tab_widget.setTabText(self.tab_widget.indexOf(self.tab_dispense), QCoreApplication.translate("OUAGWindow", u"Nutrient Dispense", None))
        self.tab_widget.setTabText(self.tab_widget.indexOf(self.tab_smart_plugs), QCoreApplication.translate("OUAGWindow", u"Smart Plugs", None))
    # retranslateUi

