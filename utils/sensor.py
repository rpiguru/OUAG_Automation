import datetime
import random

from pymongo import MongoClient

from settings import MONGO_HOST, MONGO_DB, MONGO_COLLECTION
from utils.common import get_config, is_rpi


def read_sensor_data():
    if is_rpi:
        mongo_client = MongoClient(MONGO_HOST)
        mongo_db = mongo_client[MONGO_DB]
        mongo_col = mongo_db[MONGO_COLLECTION]
        buf_list = list(mongo_col.find().sort([('ts', -1)]).limit(50))
        return buf_list
    else:
        return [dict(
            ts=datetime.datetime.now() - datetime.timedelta(seconds=i * get_config()['interval']),
            ph=random.randint(40, 90) / 10.,
            ec=random.randint(0, 20000) / 10.,
            temp=random.randint(500, 1000) / 10.,
            humidity=random.randint(0, 1000) / 10.,
            water_temp=random.randint(400, 1000) / 10.,
        ) for i in range(random.randint(30, 50))]
