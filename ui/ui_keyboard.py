# -*- coding: utf-8 -*-

################################################################################
## Form generated from reading UI file 'keyboard.ui'
##
## Created by: Qt User Interface Compiler version 5.15.1
##
## WARNING! All changes made in this file will be lost when recompiling UI file!
################################################################################

from PySide2.QtCore import *
from PySide2.QtGui import *
from PySide2.QtWidgets import *


class Ui_KeyboardDlg(object):
    def setupUi(self, KeyboardDlg):
        if not KeyboardDlg.objectName():
            KeyboardDlg.setObjectName(u"KeyboardDlg")
        KeyboardDlg.resize(800, 240)
        sizePolicy = QSizePolicy(QSizePolicy.Fixed, QSizePolicy.Fixed)
        sizePolicy.setHorizontalStretch(0)
        sizePolicy.setVerticalStretch(0)
        sizePolicy.setHeightForWidth(KeyboardDlg.sizePolicy().hasHeightForWidth())
        KeyboardDlg.setSizePolicy(sizePolicy)
        KeyboardDlg.setMinimumSize(QSize(800, 240))
        KeyboardDlg.setMaximumSize(QSize(1280, 400))
        self.verticalLayout = QVBoxLayout(KeyboardDlg)
        self.verticalLayout.setObjectName(u"verticalLayout")
        self.widget = QWidget(KeyboardDlg)
        self.widget.setObjectName(u"widget")
        font = QFont()
        font.setPointSize(12)
        self.widget.setFont(font)
        self.horizontalLayout = QHBoxLayout(self.widget)
        self.horizontalLayout.setObjectName(u"horizontalLayout")
        self.horizontalLayout.setContentsMargins(0, 0, 0, 0)
        self.btn_1 = QPushButton(self.widget)
        self.btn_1.setObjectName(u"btn_1")
        sizePolicy1 = QSizePolicy(QSizePolicy.Ignored, QSizePolicy.Minimum)
        sizePolicy1.setHorizontalStretch(0)
        sizePolicy1.setVerticalStretch(0)
        sizePolicy1.setHeightForWidth(self.btn_1.sizePolicy().hasHeightForWidth())
        self.btn_1.setSizePolicy(sizePolicy1)
        self.btn_1.setFont(font)

        self.horizontalLayout.addWidget(self.btn_1)

        self.btn_2 = QPushButton(self.widget)
        self.btn_2.setObjectName(u"btn_2")
        sizePolicy1.setHeightForWidth(self.btn_2.sizePolicy().hasHeightForWidth())
        self.btn_2.setSizePolicy(sizePolicy1)
        self.btn_2.setFont(font)

        self.horizontalLayout.addWidget(self.btn_2)

        self.btn_3 = QPushButton(self.widget)
        self.btn_3.setObjectName(u"btn_3")
        sizePolicy1.setHeightForWidth(self.btn_3.sizePolicy().hasHeightForWidth())
        self.btn_3.setSizePolicy(sizePolicy1)
        self.btn_3.setFont(font)

        self.horizontalLayout.addWidget(self.btn_3)

        self.btn_4 = QPushButton(self.widget)
        self.btn_4.setObjectName(u"btn_4")
        sizePolicy1.setHeightForWidth(self.btn_4.sizePolicy().hasHeightForWidth())
        self.btn_4.setSizePolicy(sizePolicy1)
        self.btn_4.setFont(font)

        self.horizontalLayout.addWidget(self.btn_4)

        self.btn_5 = QPushButton(self.widget)
        self.btn_5.setObjectName(u"btn_5")
        sizePolicy1.setHeightForWidth(self.btn_5.sizePolicy().hasHeightForWidth())
        self.btn_5.setSizePolicy(sizePolicy1)
        self.btn_5.setFont(font)

        self.horizontalLayout.addWidget(self.btn_5)

        self.btn_6 = QPushButton(self.widget)
        self.btn_6.setObjectName(u"btn_6")
        sizePolicy1.setHeightForWidth(self.btn_6.sizePolicy().hasHeightForWidth())
        self.btn_6.setSizePolicy(sizePolicy1)
        self.btn_6.setFont(font)

        self.horizontalLayout.addWidget(self.btn_6)

        self.btn_7 = QPushButton(self.widget)
        self.btn_7.setObjectName(u"btn_7")
        sizePolicy1.setHeightForWidth(self.btn_7.sizePolicy().hasHeightForWidth())
        self.btn_7.setSizePolicy(sizePolicy1)
        self.btn_7.setFont(font)

        self.horizontalLayout.addWidget(self.btn_7)

        self.btn_8 = QPushButton(self.widget)
        self.btn_8.setObjectName(u"btn_8")
        sizePolicy1.setHeightForWidth(self.btn_8.sizePolicy().hasHeightForWidth())
        self.btn_8.setSizePolicy(sizePolicy1)
        self.btn_8.setFont(font)

        self.horizontalLayout.addWidget(self.btn_8)

        self.btn_9 = QPushButton(self.widget)
        self.btn_9.setObjectName(u"btn_9")
        sizePolicy1.setHeightForWidth(self.btn_9.sizePolicy().hasHeightForWidth())
        self.btn_9.setSizePolicy(sizePolicy1)
        self.btn_9.setFont(font)

        self.horizontalLayout.addWidget(self.btn_9)

        self.btn_10 = QPushButton(self.widget)
        self.btn_10.setObjectName(u"btn_10")
        sizePolicy1.setHeightForWidth(self.btn_10.sizePolicy().hasHeightForWidth())
        self.btn_10.setSizePolicy(sizePolicy1)
        self.btn_10.setFont(font)

        self.horizontalLayout.addWidget(self.btn_10)

        self.btn_11 = QPushButton(self.widget)
        self.btn_11.setObjectName(u"btn_11")
        sizePolicy1.setHeightForWidth(self.btn_11.sizePolicy().hasHeightForWidth())
        self.btn_11.setSizePolicy(sizePolicy1)
        self.btn_11.setFont(font)

        self.horizontalLayout.addWidget(self.btn_11)

        self.btn_12 = QPushButton(self.widget)
        self.btn_12.setObjectName(u"btn_12")
        sizePolicy1.setHeightForWidth(self.btn_12.sizePolicy().hasHeightForWidth())
        self.btn_12.setSizePolicy(sizePolicy1)
        self.btn_12.setFont(font)

        self.horizontalLayout.addWidget(self.btn_12)

        self.btn_13 = QPushButton(self.widget)
        self.btn_13.setObjectName(u"btn_13")
        sizePolicy1.setHeightForWidth(self.btn_13.sizePolicy().hasHeightForWidth())
        self.btn_13.setSizePolicy(sizePolicy1)
        self.btn_13.setFont(font)

        self.horizontalLayout.addWidget(self.btn_13)

        self.btn_14 = QPushButton(self.widget)
        self.btn_14.setObjectName(u"btn_14")
        sizePolicy2 = QSizePolicy(QSizePolicy.Preferred, QSizePolicy.Minimum)
        sizePolicy2.setHorizontalStretch(0)
        sizePolicy2.setVerticalStretch(0)
        sizePolicy2.setHeightForWidth(self.btn_14.sizePolicy().hasHeightForWidth())
        self.btn_14.setSizePolicy(sizePolicy2)
        self.btn_14.setMinimumSize(QSize(50, 0))
        self.btn_14.setFont(font)

        self.horizontalLayout.addWidget(self.btn_14)


        self.verticalLayout.addWidget(self.widget)

        self.widget_2 = QWidget(KeyboardDlg)
        self.widget_2.setObjectName(u"widget_2")
        self.widget_2.setFont(font)
        self.horizontalLayout_2 = QHBoxLayout(self.widget_2)
        self.horizontalLayout_2.setObjectName(u"horizontalLayout_2")
        self.horizontalLayout_2.setContentsMargins(0, 0, 0, 0)
        self.btn_15 = QPushButton(self.widget_2)
        self.btn_15.setObjectName(u"btn_15")
        sizePolicy3 = QSizePolicy(QSizePolicy.Minimum, QSizePolicy.Minimum)
        sizePolicy3.setHorizontalStretch(0)
        sizePolicy3.setVerticalStretch(0)
        sizePolicy3.setHeightForWidth(self.btn_15.sizePolicy().hasHeightForWidth())
        self.btn_15.setSizePolicy(sizePolicy3)
        self.btn_15.setFont(font)

        self.horizontalLayout_2.addWidget(self.btn_15)

        self.btn_16 = QPushButton(self.widget_2)
        self.btn_16.setObjectName(u"btn_16")
        sizePolicy3.setHeightForWidth(self.btn_16.sizePolicy().hasHeightForWidth())
        self.btn_16.setSizePolicy(sizePolicy3)
        self.btn_16.setFont(font)

        self.horizontalLayout_2.addWidget(self.btn_16)

        self.btn_17 = QPushButton(self.widget_2)
        self.btn_17.setObjectName(u"btn_17")
        sizePolicy3.setHeightForWidth(self.btn_17.sizePolicy().hasHeightForWidth())
        self.btn_17.setSizePolicy(sizePolicy3)
        self.btn_17.setFont(font)

        self.horizontalLayout_2.addWidget(self.btn_17)

        self.btn_18 = QPushButton(self.widget_2)
        self.btn_18.setObjectName(u"btn_18")
        sizePolicy3.setHeightForWidth(self.btn_18.sizePolicy().hasHeightForWidth())
        self.btn_18.setSizePolicy(sizePolicy3)
        self.btn_18.setFont(font)

        self.horizontalLayout_2.addWidget(self.btn_18)

        self.btn_19 = QPushButton(self.widget_2)
        self.btn_19.setObjectName(u"btn_19")
        sizePolicy3.setHeightForWidth(self.btn_19.sizePolicy().hasHeightForWidth())
        self.btn_19.setSizePolicy(sizePolicy3)
        self.btn_19.setFont(font)

        self.horizontalLayout_2.addWidget(self.btn_19)

        self.btn_20 = QPushButton(self.widget_2)
        self.btn_20.setObjectName(u"btn_20")
        sizePolicy3.setHeightForWidth(self.btn_20.sizePolicy().hasHeightForWidth())
        self.btn_20.setSizePolicy(sizePolicy3)
        self.btn_20.setFont(font)

        self.horizontalLayout_2.addWidget(self.btn_20)

        self.btn_21 = QPushButton(self.widget_2)
        self.btn_21.setObjectName(u"btn_21")
        sizePolicy3.setHeightForWidth(self.btn_21.sizePolicy().hasHeightForWidth())
        self.btn_21.setSizePolicy(sizePolicy3)
        self.btn_21.setFont(font)

        self.horizontalLayout_2.addWidget(self.btn_21)

        self.btn_22 = QPushButton(self.widget_2)
        self.btn_22.setObjectName(u"btn_22")
        sizePolicy3.setHeightForWidth(self.btn_22.sizePolicy().hasHeightForWidth())
        self.btn_22.setSizePolicy(sizePolicy3)
        self.btn_22.setFont(font)

        self.horizontalLayout_2.addWidget(self.btn_22)

        self.btn_23 = QPushButton(self.widget_2)
        self.btn_23.setObjectName(u"btn_23")
        sizePolicy3.setHeightForWidth(self.btn_23.sizePolicy().hasHeightForWidth())
        self.btn_23.setSizePolicy(sizePolicy3)
        self.btn_23.setFont(font)

        self.horizontalLayout_2.addWidget(self.btn_23)

        self.btn_24 = QPushButton(self.widget_2)
        self.btn_24.setObjectName(u"btn_24")
        sizePolicy3.setHeightForWidth(self.btn_24.sizePolicy().hasHeightForWidth())
        self.btn_24.setSizePolicy(sizePolicy3)
        self.btn_24.setFont(font)

        self.horizontalLayout_2.addWidget(self.btn_24)

        self.btn_25 = QPushButton(self.widget_2)
        self.btn_25.setObjectName(u"btn_25")
        sizePolicy3.setHeightForWidth(self.btn_25.sizePolicy().hasHeightForWidth())
        self.btn_25.setSizePolicy(sizePolicy3)
        self.btn_25.setFont(font)

        self.horizontalLayout_2.addWidget(self.btn_25)

        self.btn_26 = QPushButton(self.widget_2)
        self.btn_26.setObjectName(u"btn_26")
        sizePolicy3.setHeightForWidth(self.btn_26.sizePolicy().hasHeightForWidth())
        self.btn_26.setSizePolicy(sizePolicy3)
        self.btn_26.setFont(font)

        self.horizontalLayout_2.addWidget(self.btn_26)

        self.btn_27 = QPushButton(self.widget_2)
        self.btn_27.setObjectName(u"btn_27")
        sizePolicy3.setHeightForWidth(self.btn_27.sizePolicy().hasHeightForWidth())
        self.btn_27.setSizePolicy(sizePolicy3)
        self.btn_27.setFont(font)

        self.horizontalLayout_2.addWidget(self.btn_27)


        self.verticalLayout.addWidget(self.widget_2)

        self.widget_3 = QWidget(KeyboardDlg)
        self.widget_3.setObjectName(u"widget_3")
        self.widget_3.setFont(font)
        self.horizontalLayout_3 = QHBoxLayout(self.widget_3)
        self.horizontalLayout_3.setObjectName(u"horizontalLayout_3")
        self.horizontalLayout_3.setContentsMargins(0, 0, 0, 0)
        self.btn_28 = QPushButton(self.widget_3)
        self.btn_28.setObjectName(u"btn_28")
        sizePolicy3.setHeightForWidth(self.btn_28.sizePolicy().hasHeightForWidth())
        self.btn_28.setSizePolicy(sizePolicy3)

        self.horizontalLayout_3.addWidget(self.btn_28)

        self.btn_29 = QPushButton(self.widget_3)
        self.btn_29.setObjectName(u"btn_29")
        sizePolicy1.setHeightForWidth(self.btn_29.sizePolicy().hasHeightForWidth())
        self.btn_29.setSizePolicy(sizePolicy1)
        self.btn_29.setFont(font)

        self.horizontalLayout_3.addWidget(self.btn_29)

        self.btn_30 = QPushButton(self.widget_3)
        self.btn_30.setObjectName(u"btn_30")
        sizePolicy1.setHeightForWidth(self.btn_30.sizePolicy().hasHeightForWidth())
        self.btn_30.setSizePolicy(sizePolicy1)
        self.btn_30.setFont(font)

        self.horizontalLayout_3.addWidget(self.btn_30)

        self.btn_31 = QPushButton(self.widget_3)
        self.btn_31.setObjectName(u"btn_31")
        sizePolicy1.setHeightForWidth(self.btn_31.sizePolicy().hasHeightForWidth())
        self.btn_31.setSizePolicy(sizePolicy1)
        self.btn_31.setFont(font)

        self.horizontalLayout_3.addWidget(self.btn_31)

        self.btn_32 = QPushButton(self.widget_3)
        self.btn_32.setObjectName(u"btn_32")
        sizePolicy1.setHeightForWidth(self.btn_32.sizePolicy().hasHeightForWidth())
        self.btn_32.setSizePolicy(sizePolicy1)
        self.btn_32.setFont(font)

        self.horizontalLayout_3.addWidget(self.btn_32)

        self.btn_33 = QPushButton(self.widget_3)
        self.btn_33.setObjectName(u"btn_33")
        sizePolicy1.setHeightForWidth(self.btn_33.sizePolicy().hasHeightForWidth())
        self.btn_33.setSizePolicy(sizePolicy1)
        self.btn_33.setFont(font)

        self.horizontalLayout_3.addWidget(self.btn_33)

        self.btn_34 = QPushButton(self.widget_3)
        self.btn_34.setObjectName(u"btn_34")
        sizePolicy1.setHeightForWidth(self.btn_34.sizePolicy().hasHeightForWidth())
        self.btn_34.setSizePolicy(sizePolicy1)
        self.btn_34.setFont(font)

        self.horizontalLayout_3.addWidget(self.btn_34)

        self.btn_35 = QPushButton(self.widget_3)
        self.btn_35.setObjectName(u"btn_35")
        sizePolicy1.setHeightForWidth(self.btn_35.sizePolicy().hasHeightForWidth())
        self.btn_35.setSizePolicy(sizePolicy1)
        self.btn_35.setFont(font)

        self.horizontalLayout_3.addWidget(self.btn_35)

        self.btn_36 = QPushButton(self.widget_3)
        self.btn_36.setObjectName(u"btn_36")
        sizePolicy1.setHeightForWidth(self.btn_36.sizePolicy().hasHeightForWidth())
        self.btn_36.setSizePolicy(sizePolicy1)
        self.btn_36.setFont(font)

        self.horizontalLayout_3.addWidget(self.btn_36)

        self.btn_37 = QPushButton(self.widget_3)
        self.btn_37.setObjectName(u"btn_37")
        sizePolicy1.setHeightForWidth(self.btn_37.sizePolicy().hasHeightForWidth())
        self.btn_37.setSizePolicy(sizePolicy1)
        self.btn_37.setFont(font)

        self.horizontalLayout_3.addWidget(self.btn_37)

        self.btn_38 = QPushButton(self.widget_3)
        self.btn_38.setObjectName(u"btn_38")
        sizePolicy1.setHeightForWidth(self.btn_38.sizePolicy().hasHeightForWidth())
        self.btn_38.setSizePolicy(sizePolicy1)
        self.btn_38.setFont(font)

        self.horizontalLayout_3.addWidget(self.btn_38)

        self.btn_39 = QPushButton(self.widget_3)
        self.btn_39.setObjectName(u"btn_39")
        sizePolicy1.setHeightForWidth(self.btn_39.sizePolicy().hasHeightForWidth())
        self.btn_39.setSizePolicy(sizePolicy1)
        self.btn_39.setFont(font)

        self.horizontalLayout_3.addWidget(self.btn_39)

        self.btn_40 = QPushButton(self.widget_3)
        self.btn_40.setObjectName(u"btn_40")
        sizePolicy3.setHeightForWidth(self.btn_40.sizePolicy().hasHeightForWidth())
        self.btn_40.setSizePolicy(sizePolicy3)
        self.btn_40.setMinimumSize(QSize(80, 0))
        self.btn_40.setFont(font)

        self.horizontalLayout_3.addWidget(self.btn_40)


        self.verticalLayout.addWidget(self.widget_3)

        self.widget_4 = QWidget(KeyboardDlg)
        self.widget_4.setObjectName(u"widget_4")
        self.widget_4.setFont(font)
        self.horizontalLayout_4 = QHBoxLayout(self.widget_4)
        self.horizontalLayout_4.setObjectName(u"horizontalLayout_4")
        self.horizontalLayout_4.setContentsMargins(0, 0, 0, 0)
        self.btn_41 = QPushButton(self.widget_4)
        self.btn_41.setObjectName(u"btn_41")
        sizePolicy3.setHeightForWidth(self.btn_41.sizePolicy().hasHeightForWidth())
        self.btn_41.setSizePolicy(sizePolicy3)
        self.btn_41.setMinimumSize(QSize(70, 0))
        self.btn_41.setFont(font)

        self.horizontalLayout_4.addWidget(self.btn_41)

        self.btn_42 = QPushButton(self.widget_4)
        self.btn_42.setObjectName(u"btn_42")
        sizePolicy1.setHeightForWidth(self.btn_42.sizePolicy().hasHeightForWidth())
        self.btn_42.setSizePolicy(sizePolicy1)
        self.btn_42.setFont(font)

        self.horizontalLayout_4.addWidget(self.btn_42)

        self.btn_43 = QPushButton(self.widget_4)
        self.btn_43.setObjectName(u"btn_43")
        sizePolicy1.setHeightForWidth(self.btn_43.sizePolicy().hasHeightForWidth())
        self.btn_43.setSizePolicy(sizePolicy1)
        self.btn_43.setFont(font)

        self.horizontalLayout_4.addWidget(self.btn_43)

        self.btn_44 = QPushButton(self.widget_4)
        self.btn_44.setObjectName(u"btn_44")
        sizePolicy1.setHeightForWidth(self.btn_44.sizePolicy().hasHeightForWidth())
        self.btn_44.setSizePolicy(sizePolicy1)
        self.btn_44.setFont(font)

        self.horizontalLayout_4.addWidget(self.btn_44)

        self.btn_45 = QPushButton(self.widget_4)
        self.btn_45.setObjectName(u"btn_45")
        sizePolicy1.setHeightForWidth(self.btn_45.sizePolicy().hasHeightForWidth())
        self.btn_45.setSizePolicy(sizePolicy1)
        self.btn_45.setFont(font)

        self.horizontalLayout_4.addWidget(self.btn_45)

        self.btn_46 = QPushButton(self.widget_4)
        self.btn_46.setObjectName(u"btn_46")
        sizePolicy1.setHeightForWidth(self.btn_46.sizePolicy().hasHeightForWidth())
        self.btn_46.setSizePolicy(sizePolicy1)
        self.btn_46.setFont(font)

        self.horizontalLayout_4.addWidget(self.btn_46)

        self.btn_47 = QPushButton(self.widget_4)
        self.btn_47.setObjectName(u"btn_47")
        sizePolicy1.setHeightForWidth(self.btn_47.sizePolicy().hasHeightForWidth())
        self.btn_47.setSizePolicy(sizePolicy1)
        self.btn_47.setFont(font)

        self.horizontalLayout_4.addWidget(self.btn_47)

        self.btn_48 = QPushButton(self.widget_4)
        self.btn_48.setObjectName(u"btn_48")
        sizePolicy1.setHeightForWidth(self.btn_48.sizePolicy().hasHeightForWidth())
        self.btn_48.setSizePolicy(sizePolicy1)
        self.btn_48.setFont(font)

        self.horizontalLayout_4.addWidget(self.btn_48)

        self.btn_49 = QPushButton(self.widget_4)
        self.btn_49.setObjectName(u"btn_49")
        sizePolicy1.setHeightForWidth(self.btn_49.sizePolicy().hasHeightForWidth())
        self.btn_49.setSizePolicy(sizePolicy1)
        self.btn_49.setFont(font)

        self.horizontalLayout_4.addWidget(self.btn_49)

        self.btn_50 = QPushButton(self.widget_4)
        self.btn_50.setObjectName(u"btn_50")
        sizePolicy1.setHeightForWidth(self.btn_50.sizePolicy().hasHeightForWidth())
        self.btn_50.setSizePolicy(sizePolicy1)
        self.btn_50.setFont(font)

        self.horizontalLayout_4.addWidget(self.btn_50)

        self.btn_51 = QPushButton(self.widget_4)
        self.btn_51.setObjectName(u"btn_51")
        sizePolicy1.setHeightForWidth(self.btn_51.sizePolicy().hasHeightForWidth())
        self.btn_51.setSizePolicy(sizePolicy1)
        self.btn_51.setFont(font)

        self.horizontalLayout_4.addWidget(self.btn_51)

        self.btn_52 = QPushButton(self.widget_4)
        self.btn_52.setObjectName(u"btn_52")
        sizePolicy3.setHeightForWidth(self.btn_52.sizePolicy().hasHeightForWidth())
        self.btn_52.setSizePolicy(sizePolicy3)
        self.btn_52.setMinimumSize(QSize(70, 0))
        self.btn_52.setFont(font)

        self.horizontalLayout_4.addWidget(self.btn_52)


        self.verticalLayout.addWidget(self.widget_4)

        self.widget_5 = QWidget(KeyboardDlg)
        self.widget_5.setObjectName(u"widget_5")
        self.horizontalLayout_5 = QHBoxLayout(self.widget_5)
        self.horizontalLayout_5.setObjectName(u"horizontalLayout_5")
        self.horizontalLayout_5.setContentsMargins(0, 0, 0, 0)
        self.btn_0 = QPushButton(self.widget_5)
        self.btn_0.setObjectName(u"btn_0")
        sizePolicy3.setHeightForWidth(self.btn_0.sizePolicy().hasHeightForWidth())
        self.btn_0.setSizePolicy(sizePolicy3)

        self.horizontalLayout_5.addWidget(self.btn_0)


        self.verticalLayout.addWidget(self.widget_5)


        self.retranslateUi(KeyboardDlg)

        QMetaObject.connectSlotsByName(KeyboardDlg)
    # setupUi

    def retranslateUi(self, KeyboardDlg):
        KeyboardDlg.setWindowTitle(QCoreApplication.translate("KeyboardDlg", u"Keyboard", None))
        self.btn_1.setText(QCoreApplication.translate("KeyboardDlg", u"`", None))
        self.btn_2.setText(QCoreApplication.translate("KeyboardDlg", u"1", None))
        self.btn_3.setText(QCoreApplication.translate("KeyboardDlg", u"2", None))
        self.btn_4.setText(QCoreApplication.translate("KeyboardDlg", u"3", None))
        self.btn_5.setText(QCoreApplication.translate("KeyboardDlg", u"4", None))
        self.btn_6.setText(QCoreApplication.translate("KeyboardDlg", u"5", None))
        self.btn_7.setText(QCoreApplication.translate("KeyboardDlg", u"6", None))
        self.btn_8.setText(QCoreApplication.translate("KeyboardDlg", u"7", None))
        self.btn_9.setText(QCoreApplication.translate("KeyboardDlg", u"8", None))
        self.btn_10.setText(QCoreApplication.translate("KeyboardDlg", u"9", None))
        self.btn_11.setText(QCoreApplication.translate("KeyboardDlg", u"0", None))
        self.btn_12.setText(QCoreApplication.translate("KeyboardDlg", u"-", None))
        self.btn_13.setText(QCoreApplication.translate("KeyboardDlg", u"=", None))
        self.btn_14.setText(QCoreApplication.translate("KeyboardDlg", u"\u232b", None))
        self.btn_15.setText(QCoreApplication.translate("KeyboardDlg", u"q", None))
        self.btn_16.setText(QCoreApplication.translate("KeyboardDlg", u"w", None))
        self.btn_17.setText(QCoreApplication.translate("KeyboardDlg", u"e", None))
        self.btn_18.setText(QCoreApplication.translate("KeyboardDlg", u"r", None))
        self.btn_19.setText(QCoreApplication.translate("KeyboardDlg", u"t", None))
        self.btn_20.setText(QCoreApplication.translate("KeyboardDlg", u"y", None))
        self.btn_21.setText(QCoreApplication.translate("KeyboardDlg", u"u", None))
        self.btn_22.setText(QCoreApplication.translate("KeyboardDlg", u"i", None))
        self.btn_23.setText(QCoreApplication.translate("KeyboardDlg", u"o", None))
        self.btn_24.setText(QCoreApplication.translate("KeyboardDlg", u"p", None))
        self.btn_25.setText(QCoreApplication.translate("KeyboardDlg", u"[", None))
        self.btn_26.setText(QCoreApplication.translate("KeyboardDlg", u"]", None))
        self.btn_27.setText(QCoreApplication.translate("KeyboardDlg", u"\\", None))
        self.btn_28.setText(QCoreApplication.translate("KeyboardDlg", u"caps", None))
        self.btn_29.setText(QCoreApplication.translate("KeyboardDlg", u"a", None))
        self.btn_30.setText(QCoreApplication.translate("KeyboardDlg", u"s", None))
        self.btn_31.setText(QCoreApplication.translate("KeyboardDlg", u"d", None))
        self.btn_32.setText(QCoreApplication.translate("KeyboardDlg", u"f", None))
        self.btn_33.setText(QCoreApplication.translate("KeyboardDlg", u"g", None))
        self.btn_34.setText(QCoreApplication.translate("KeyboardDlg", u"h", None))
        self.btn_35.setText(QCoreApplication.translate("KeyboardDlg", u"j", None))
        self.btn_36.setText(QCoreApplication.translate("KeyboardDlg", u"k", None))
        self.btn_37.setText(QCoreApplication.translate("KeyboardDlg", u"l", None))
        self.btn_38.setText(QCoreApplication.translate("KeyboardDlg", u";", None))
        self.btn_39.setText(QCoreApplication.translate("KeyboardDlg", u"'", None))
        self.btn_40.setText(QCoreApplication.translate("KeyboardDlg", u"enter", None))
        self.btn_41.setText(QCoreApplication.translate("KeyboardDlg", u"shift", None))
        self.btn_42.setText(QCoreApplication.translate("KeyboardDlg", u"z", None))
        self.btn_43.setText(QCoreApplication.translate("KeyboardDlg", u"x", None))
        self.btn_44.setText(QCoreApplication.translate("KeyboardDlg", u"c", None))
        self.btn_45.setText(QCoreApplication.translate("KeyboardDlg", u"v", None))
        self.btn_46.setText(QCoreApplication.translate("KeyboardDlg", u"b", None))
        self.btn_47.setText(QCoreApplication.translate("KeyboardDlg", u"n", None))
        self.btn_48.setText(QCoreApplication.translate("KeyboardDlg", u"m", None))
        self.btn_49.setText(QCoreApplication.translate("KeyboardDlg", u",", None))
        self.btn_50.setText(QCoreApplication.translate("KeyboardDlg", u".", None))
        self.btn_51.setText(QCoreApplication.translate("KeyboardDlg", u"/", None))
        self.btn_52.setText(QCoreApplication.translate("KeyboardDlg", u"shift", None))
        self.btn_0.setText("")
    # retranslateUi

