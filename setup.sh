echo "========== Starting Installation of OUAG Automation =========="

cur_dir="$( cd "$(dirname "$0")" ; pwd -P )"
user="$(id -u -n)"

# Configuring git
sed -i -- "s/https:\/\/gitlab.com/https:\/\/deploy:eorsPVz5rzzykyXxRzMF@gitlab.com/g" .git/config

sudo apt update

sudo apt install -y python3-dev python3-pip build-essential cmake mongodb
sudo mkdir /data && sudo chmod 777 -R /data && mkdir /data/db && sudo systemctl enable mongodb && sudo systemctl start mongodb

# Install PySide2
sudo apt install -y python3-pyside2.qt3dcore python3-pyside2.qt3dinput python3-pyside2.qt3dlogic \
                    python3-pyside2.qt3drender python3-pyside2.qtcharts python3-pyside2.qtconcurrent \
                    python3-pyside2.qtcore python3-pyside2.qtgui python3-pyside2.qthelp python3-pyside2.qtlocation \
                    python3-pyside2.qtmultimedia python3-pyside2.qtmultimediawidgets python3-pyside2.qtnetwork \
                    python3-pyside2.qtopengl python3-pyside2.qtpositioning python3-pyside2.qtprintsupport \
                    python3-pyside2.qtqml python3-pyside2.qtquick python3-pyside2.qtquickwidgets \
                    python3-pyside2.qtscript python3-pyside2.qtscripttools python3-pyside2.qtsensors \
                    python3-pyside2.qtsql python3-pyside2.qtsvg python3-pyside2.qttest python3-pyside2.qttexttospeech \
                    python3-pyside2.qtuitools python3-pyside2.qtwebchannel python3-pyside2.qtwebsockets \
                    python3-pyside2.qtwidgets python3-pyside2.qtx11extras python3-pyside2.qtxml \
                    python3-pyside2.qtxmlpatterns python3-pyside2uic pyside2-tools qml-module-qtquick-virtualkeyboard matchbox-keyboard
sudo sed -i -- "s/#! \/usr\/bin\/python2/#! \/usr\/bin\/python3/g" /usr/bin/pyside2-uic

sudo python3 -m pip install -U pip setuptools wheel
sudo python3 -m pip install -r ${cur_dir}/requirements.txt

# Install opencv dependencies
sudo apt install -y libqtgui4 python3-pyqt5 libatlas-base-dev libavformat-dev libswscale-dev libjasper-dev\
                    libqt4-test libjpeg-dev libhdf5-dev libilmbase-dev libopenexr-dev libgstreamer1.0-dev

# Remove SSH warning
sudo apt purge -y libpam-chksshpwd

# Enable SSH
sudo touch /boot/ssh

# Install anydesk
wget https://download.anydesk.com/rpi/anydesk_6.1.0-1_armhf.deb
sudo dpkg -i anydesk_6.1.0-1_armhf.deb && sudo apt -f -y install
echo "ouag_automation" | sudo anydesk --set-password

# Increase GPU memory size
echo "gpu_mem=384" | sudo tee -a /boot/config.txt

echo "Disabling the booting logo and install custom splash..."
echo "disable_splash=1" | sudo tee -a /boot/config.txt
sudo sed -i -- "s/$/ logo.nologo quiet loglevel=3 vt.global_cursor_default=0 systemd.show_status=0 plymouth.ignore-serial-consoles plymouth.enable=0/" /boot/cmdline.txt
sudo sed -i -- "s/console=tty1/console=tty3/" /boot/cmdline.txt
sudo apt install -y fbi
cp ${cur_dir}/ui/img/splash.jpg /home/pi/
sudo cp ${cur_dir}/scripts/splashscreen.service /etc/systemd/system/splashscreen.service
sudo systemctl enable splashscreen

# Disable the login prompt
sudo systemctl disable getty@tty1

# Disable some services to reduce booting time
sudo systemctl disable hciuart
sudo mkdir /etc/systemd/system/networking.service.d
sudo touch /etc/systemd/system/networking.service.d/reduce-timeout.conf
echo "[Service]" | sudo tee -a /etc/systemd/system/networking.service.d/reduce-timeout.conf
echo "TimeoutStartSec=1" | sudo tee -a /etc/systemd/system/networking.service.d/reduce-timeout.conf
sudo rm /etc/systemd/system/dhcpcd.service.d/wait.conf

# Auto-mount USB drives
sudo apt install -y pmount
sudo cp ${cur_dir}/scripts/usbstick.rules /etc/udev/rules.d/
sudo cp ${cur_dir}/scripts/usbstick-handler@.service /etc/systemd/system/
sudo cp ${cur_dir}/scripts/cpmount /usr/local/bin/
sudo chmod u+x /usr/local/bin/cpmount

# Clear desktop
sudo sed -i -- "s/show_trash=1/#show_trash=1/" /etc/xdg/pcmanfm/LXDE-pi/desktop-items-0.conf
sudo sed -i -- "s/show_mounts=1/#show_mounts=1/" /etc/xdg/pcmanfm/LXDE-pi/desktop-items-0.conf
# Disable welcome wizard
sudo rm /etc/xdg/autostart/piwiz.desktop

# Set wallpaper
sudo sed -i -- "s/wallpaper=/#wallpaper=/" /etc/xdg/pcmanfm/LXDE-pi/desktop-items-0.conf
echo "wallpaper=/home/pi/splash.jpg" | sudo tee -a /etc/xdg/pcmanfm/LXDE-pi/desktop-items-0.conf

# Hide menu bar and others
sudo sed -i -- "s/@lxpanel --profile LXDE-pi/#@lxpanel --profile LXDE-pi/" /etc/xdg/lxsession/LXDE-pi/autostart
echo "xset s off" | sudo tee -a /etc/xdg/lxsession/LXDE-pi/autostart
echo "xset -dpms" | sudo tee -a /etc/xdg/lxsession/LXDE-pi/autostart
#echo "@unclutter -idle 0" | sudo tee -a /etc/xdg/lxsession/LXDE-pi/autostart

echo "max_usb_current=1" | sudo tee -a /boot/config.txt

# Enable I2C for the ADS1115
sudo apt install -y python3-smbus i2c-tools
sudo sed -i -- "s/#dtparam=i2c_arm=on/dtparam=i2c_arm=on/g" /boot/config.txt

# Enable One-Wire
echo "dtoverlay=w1-gpio" | sudo tee -a /boot/config.txt

# Enable PiCamera
echo "start_x=1" | sudo tee -a /boot/config.txt

# Configure 7" waveshare touchscreen
echo "max_usb_current=1" | sudo tee -a /boot/config.txt
echo "hdmi_group=2" | sudo tee -a /boot/config.txt
echo "hdmi_mode=87" | sudo tee -a /boot/config.txt
echo "hdmi_cvt 1024 600 60 6 0 0 0" | sudo tee -a /boot/config.txt
echo "hdmi_drive=1" | sudo tee -a /boot/config.txt

python3 ${cur_dir}/ui/compile_ui.py

# Enable Auto Start
sudo apt install -y screen
sudo sed -i -- "s/^exit 0/su ${user} -c \'screen -mS ouag -d\'\\nexit 0/g" /etc/rc.local
sudo sed -i -- "s/^exit 0/su ${user} -c \'screen -S ouag -X stuff \"export LD_PRELOAD=\/usr\/lib\/arm-linux-gnueabihf\/libatomic.so.1\\\\r\"\'\\nexit 0/g" /etc/rc.local
sudo sed -i -- "s/^exit 0/su ${user} -c \'screen -S ouag -X stuff \"export DISPLAY=:0.0\\\\r\"\'\\nexit 0/g" /etc/rc.local
sudo sed -i -- "s/^exit 0/su ${user} -c \'screen -S ouag -X stuff \"cd ${cur_dir////\\/}\\\\r\"\'\\nexit 0/g" /etc/rc.local
sudo sed -i -- "s/^exit 0/su ${user} -c \'screen -S ouag -X stuff \"sleep 3 \&\& python3 main.py\\\\r\"\'\\nexit 0/g" /etc/rc.local

sudo sed -i -- "s/^exit 0/su ${user} -c \'screen -mS b -d\'\\nexit 0/g" /etc/rc.local
sudo sed -i -- "s/^exit 0/su ${user} -c \'screen -S b -X stuff \"cd ${cur_dir////\\/}\\\\r\"\'\\nexit 0/g" /etc/rc.local
sudo sed -i -- "s/^exit 0/su ${user} -c \'screen -S b -X stuff \"python3 backend.py\\\\r\"\'\\nexit 0/g" /etc/rc.local
