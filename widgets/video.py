import ntpath
import os
import time
from json import JSONDecodeError
from queue import Queue

import qimage2ndarray
import numpy as np
import threading

import cv2
from PySide2 import QtGui
from PySide2.QtCore import QTimer, QSize, Signal
from PySide2.QtGui import QPixmap, QIcon
from PySide2.QtWidgets import QLabel

from settings import CAMERA_STRINGS, SCREEN_SIZE
from ui.ui_video_file_widget import Ui_VideoFileWidget
from utils.common import get_config, logger

_cur_dir = os.path.dirname(os.path.realpath(__file__))


class VideoStream(threading.Thread):

    def __init__(self, index=0, size=SCREEN_SIZE, queue=None):
        super().__init__()
        self.index = index
        self._b_stop = threading.Event()
        self._b_stop.clear()
        self._b_pause = threading.Event()
        self._b_pause.clear()
        self.size = size
        self._cap = None
        self._last_active_time = 0
        self._last_msg_time = 0
        self.frame_queue = queue
        self._zoom = 1

    def run(self) -> None:
        connecting_img = get_config()['settings']['connecting_splash_image']
        if os.path.isfile(connecting_img):
            con_frame = cv2.resize(cv2.imread(connecting_img), self.size)
        else:
            con_frame = None

        while not self._b_stop.is_set():
            try:
                conf = get_config()['cameras'][self.index]
            except JSONDecodeError:
                continue
            if conf.get('enabled', True):
                if self._cap is not None or self._start_camera():
                    try:
                        ret, frame = self._cap.read()
                    except Exception as e:
                        logger.error(f"Failed to read video frame from {self.index} - {e}")
                        self._cap = None
                        continue
                    if ret:
                        if not self._b_pause.is_set():
                            # Apply zoom
                            frame = cv2.resize(frame, (int(self.size[0] * self._zoom), int(self.size[1] * self._zoom)))
                            h, w = frame.shape[:2]
                            offset_w, offset_h = (w - self.size[0]) // 2, (h - self.size[1]) // 2
                            frame = frame[offset_h: offset_h + self.size[1], offset_w: offset_w + self.size[0]]
                            if not self.frame_queue.full():
                                self.frame_queue.put(frame)
                            self._last_active_time = time.time()
                    elif time.time() - self._last_active_time > 3:
                        if time.time() - self._last_msg_time > 60:
                            logger.error(f"Failed to grab video frame from camera{self.index} - {conf['url']}")
                            self._last_msg_time = time.time()
                        if con_frame is not None:
                            self.frame_queue.put(con_frame)
                        self._cap = None
                else:
                    time.sleep(1)
            else:
                time.sleep(1)

    def _start_camera(self):
        conf = get_config()['cameras'][self.index]
        url = CAMERA_STRINGS[conf['type']]['rtsp'].format(url=conf['url'])
        logger.info(f">>> Starting camera service({self.index}) - {url}")
        self._cap = cv2.VideoCapture(url)
        ret, frame = self._cap.read()
        if not ret:
            self._cap = None
            return
        logger.info(f">>> Camera {self.index}: STARTED")
        return True

    def set_zoom(self, val):
        self._zoom = val

    def pause(self):
        self._b_pause.set()

    def resume(self):
        self._b_pause.clear()

    def stop(self):
        self._b_stop.set()


class VideoWidget(QLabel):

    def __init__(self, parent):
        super().__init__(parent)
        self._parent = parent
        self._frame = None
        self._stopped = False
        self._queues = [Queue(maxsize=1) for _ in range(2)]
        self._streams = [VideoStream(index=i, queue=self._queues[i]) for i in range(2)]
        connecting_img = get_config()['settings']['connecting_splash_image']
        if os.path.isfile(connecting_img):
            self._frames = [cv2.resize(cv2.imread(connecting_img), SCREEN_SIZE) for _ in range(2)]
        else:
            self._frames = [np.zeros((SCREEN_SIZE[1], SCREEN_SIZE[0], 3), dtype=np.uint8) for _ in range(2)]
        self.timer = QTimer()
        self.timer.timeout.connect(self._display_video_stream)
        self.mode = "1"
        self.pip_master = 0
        self.set_mode("1")

    def start(self):
        for s in self._streams:
            s.start()
        self.timer.start(30)

    def _display_video_stream(self):
        if self._stopped:
            return
        for i, q in enumerate(self._queues):
            if not q.empty():
                self._frames[i] = q.get()
        if self.mode == "1":
            frame = self._frames[0]
        elif self.mode == "2":
            frame = self._frames[1]
        elif self.mode == 'sbs':
            frame = cv2.hconcat(
                [cv2.resize(np.rot90(f), (SCREEN_SIZE[0] // 2, SCREEN_SIZE[1])) for f in self._frames])
        else:  # pip
            m_frame = self._frames[0] if self.pip_master == 0 else self._frames[1]
            s_frame = self._frames[1] if self.pip_master == 0 else self._frames[0]
            w, h = int(SCREEN_SIZE[0] * .3), int(SCREEN_SIZE[1] * .3)
            s_frame = cv2.resize(s_frame, (w, h))
            x, y = int(SCREEN_SIZE[0] * .7), int(SCREEN_SIZE[1] * .7)
            m_frame[y: y + h, x: x + w] = s_frame
            frame = m_frame

        self._frame = frame.copy()
        frame = cv2.cvtColor(frame, cv2.COLOR_BGR2RGB)
        img = qimage2ndarray.array2qimage(frame)
        self.setPixmap(QPixmap.fromImage(img))

    def set_mode(self, mode):
        self.mode = mode
        if mode == '1':
            self._streams[0].resume()
            self._streams[1].pause()
        elif self.mode == "2":
            self._streams[0].pause()
            self._streams[1].resume()
        else:
            self._streams[0].resume()
            self._streams[1].resume()
            if self.mode == 'pip':
                self.pip_master = 0 if self.pip_master == 1 else 1

    def get_frame(self):
        return self._frame

    def hideEvent(self, event: QtGui.QHideEvent):
        self.timer.stop()
        for s in self._streams:
            s.stop()
        super(VideoWidget, self).hideEvent(event)

    def set_zoom(self, index, val):
        self._streams[index].set_zoom(val)

    def pause(self):
        self._stopped = True
        for s in self._streams:
            s.pause()

    def resume(self):
        self._stopped = False
        for s in self._streams:
            s.resume()


class VideoFileWidget(QLabel):

    max_len = Signal(int)

    def __init__(self, parent, path=""):
        super().__init__(parent)
        self.ui = Ui_VideoFileWidget()
        self.ui.setupUi(self)
        self.path = path
        self._playing = False
        self.ui.btn.released.connect(self._on_btn)
        if path:
            self.set_path(path)
        self.ui.horizontalSlider.valueChanged.connect(self._on_slider_changed)
        self._stopped = False
        self._cnt = 0
        self._queue = Queue(maxsize=1)
        self._tr = threading.Thread(target=self._grab_video, daemon=True)
        self._tr.start()
        self.timer = QTimer()
        self.timer.timeout.connect(self._display_video_stream)
        self.timer.start(33)
        self.max_len.connect(self._on_max_len)

    def set_path(self, path):
        self.path = path
        self.ui.lbTitle.setText(ntpath.basename(path))
        self.ui.horizontalSlider.setValue(0)
        self._playing = True

    def _grab_video(self):
        bad_frame = cv2.imread(os.path.join(_cur_dir, '../ui/img/bad_camera.png'))
        path = None
        _cap = None
        self._cnt = 0
        while not self._stopped:
            if path != self.path:
                if path:
                    logger.info(f"Playing video file - {self.path}")
                if _cap is not None:
                    _cap.release()
                    _cap = None
                _cap = cv2.VideoCapture(path)
                self.max_len.emit(_cap.get(cv2.CAP_PROP_FRAME_COUNT))
                self._cnt = 0
                path = self.path

            if _cap is not None:
                ret, frame = _cap.read()
                if ret:
                    self._cnt += 1
                    if self._cnt >= _cap.get(cv2.CAP_PROP_FRAME_COUNT):
                        self._cnt = 0
                        _cap.set(cv2.CAP_PROP_POS_FRAMES, 0)
                else:
                    if path:
                        logger.error(f"Failed to read video frame - {path}")
                    _cap.release()
                    time.sleep(.1)
                    _cap = cv2.VideoCapture(path)
                    self._cnt = 0
                    self.max_len.emit(_cap.get(cv2.CAP_PROP_FRAME_COUNT))
                    frame = bad_frame
                frame = cv2.resize(frame, (self.width(), self.height()))
                frame = cv2.cvtColor(frame, cv2.COLOR_BGR2RGB)
                if not self._queue.full():
                    self._queue.put((self._cnt, frame))
            time.sleep(.02)
        if _cap is not None:
            _cap.release()

    def _on_max_len(self, val):
        self.ui.horizontalSlider.setMaximum(val)

    def _display_video_stream(self):
        if self._queue.empty():
            return
        cnt, frame = self._queue.get()
        self.ui.horizontalSlider.setValue(cnt)
        if self._playing:
            img = qimage2ndarray.array2qimage(frame)
            self.ui.video.setPixmap(QPixmap.fromImage(img))

    def _on_btn(self):
        self._playing = not self._playing
        icon = QIcon()
        icon.addFile(f":/img/img/{'pause' if self._playing else 'play'}_blue.png", QSize(), QIcon.Normal, QIcon.Off)
        self.ui.btn.setIcon(icon)

    def _on_slider_changed(self):
        self._cnt = self.ui.horizontalSlider.value()

    def closeEvent(self, *args, **kwargs):
        self.timer.stop()
        self._stopped = True
        self._tr.join(.1)
        super(VideoFileWidget, self).closeEvent(*args, **kwargs)
