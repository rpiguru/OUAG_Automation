import json
import os
import subprocess
import signal
import gc
import sys
import threading

from settings import CONFIG_FILE
from utils.logger import logger

_cur_dir = os.path.dirname(os.path.realpath(__file__))
_par_dir = os.path.join(_cur_dir, os.path.pardir)
if _par_dir not in sys.path:
    sys.path.append(_par_dir)


try:
    is_rpi = 'arm' in os.uname()[4]
except AttributeError:
    is_rpi = False


def get_serial():
    """
    Get serial number of the device
    :return:
    """
    if is_rpi:
        cpuserial = "0000000000000000"
        f = open('/proc/cpuinfo', 'r')
        for line in f:
            if line[0:6] == 'Serial':
                cpuserial = line[10:26].lstrip('0')
        f.close()
        return cpuserial
    else:
        return '10000000ba29d58e'


def number_to_ordinal(n):
    """
    Convert number to ordinal number string
    """
    return "%d%s" % (n, "tsnrhtdd"[(n / 10 % 10 != 1) * (n % 10 < 4) * n % 10::4])


def get_free_gpu_size():
    gc.collect()
    if is_rpi:
        try:
            pipe = os.popen('sudo vcdbg reloc stats | grep "free memory"')
            data = pipe.read().strip()
            pipe.close()
            return data
        except OSError:
            logger.error('!!! Failed to get free GPU size! ')
            tot, used, free = map(int, os.popen('free -t -m').readlines()[-1].split()[1:])
            logger.error('Total: {}, Used: {}, Free: {}'.format(tot, used, free))
    return 0


def disable_screen_saver():
    if is_rpi:
        os.system('sudo sh -c "TERM=linux setterm -blank 0 >/dev/tty0"')


def check_running_proc(proc_name):
    """
    Check if a process is running or not
    :param proc_name:
    :return:
    """
    is_running = False
    cmd = "ps -aef | grep -i '%s' | grep -v 'grep' | awk '{ print $3 }'" % proc_name
    try:
        if len(os.popen(cmd).read().strip().splitlines()) > 0:
            is_running = True
    except Exception as e:
        print(f'Failed to get status of the process({proc_name}) - {e}')
    return is_running


def kill_process_by_name(proc_name, args="", use_sudo=True, sig=None):
    sig = sig if sig is not None else signal.SIGKILL
    p = subprocess.Popen(['ps', '-efww'], stdout=subprocess.PIPE)
    out, err = p.communicate()
    for line in out.decode().splitlines():
        if proc_name in line and (not args or args in line):
            pid = int(line.split()[1])
            logger.info(f'Found PID({pid}) of `{proc_name}`, killing with {sig}...')
            if use_sudo:
                os.system(f'sudo kill -{sig} {pid}')
            else:
                os.kill(pid, sig)


def update_dict_recursively(dest, updated):
    """
    Update dictionary recursively.
    :param dest: Destination dict.
    :type dest: dict
    :param updated: Updated dict to be applied.
    :type updated: dict
    :return:
    """
    for k, v in updated.items():
        if isinstance(dest, dict):
            if isinstance(v, dict):
                r = update_dict_recursively(dest.get(k, {}), v)
                dest[k] = r
            else:
                dest[k] = updated[k]
        else:
            dest = {k: updated[k]}
    return dest


lock = threading.Lock()


def update_config_file(data):
    old_data = update_dict_recursively(dest=get_config(), updated=data)
    with lock:
        with open(CONFIG_FILE, 'w') as jp:
            json.dump(old_data, jp, indent=2)


def get_config():
    with lock:
        try:
            conf = json.loads(open(CONFIG_FILE).read())
        except Exception as e:
            logger.error(f"Failed to read config file ({e})")
    return conf


def repair_mongodb():
    os.system("sudo rm /var/lib/mongodb/*.ns")
    os.system("sudo rm /var/lib/mongodb/mongod.lock")
    os.system("sudo rm /tmp/mongodb-27017.sock")
    _p = subprocess.Popen('sudo -u mongodb mongod --repair --dbpath /var/lib/mongodb/', shell=True,
                          stdout=subprocess.PIPE, stderr=subprocess.PIPE)
    _p.communicate()
    _p.wait()
    subprocess.Popen('sudo service mongodb restart', shell=True)


def create_wifi_config(ssid, password):
    config_lines = [
        f'country=US',
        'ctrl_interface=DIR=/var/run/wpa_supplicant GROUP=netdev',
        'update_config=1'
        '\n',
        'network={',
        '\tssid="{}"'.format(ssid),
        '\tpsk="{}"'.format(password),
        '\tkey_mgmt=WPA-PSK',
        '}'
    ]
    config = '\n'.join(config_lines)

    if is_rpi:
        with open("/tmp/wifi.conf", "w") as wifi:
            wifi.write(config)
        os.system("sudo mv /tmp/wifi.conf /etc/wpa_supplicant/wpa_supplicant.conf")
    else:
        print(config)


def scan_wifi_networks():
    if is_rpi:
        try:
            iwlist_raw = subprocess.Popen(['sudo', '/sbin/iwlist', 'wlan0', 'scan'], stdout=subprocess.PIPE)
            ap_list, err = iwlist_raw.communicate()
            ap_array = []
            for line in ap_list.decode('utf-8').rsplit('\n'):
                if 'ESSID' in line:
                    ap_ssid = line[27:-1]
                    if ap_ssid != '' and ap_ssid not in ap_array and "\\x" not in ap_ssid:
                        ap_array.append(ap_ssid)
            return sorted(ap_array)
        except Exception as e:
            logger.error(f"Failed to scan wifi: {e}")
            return []
    else:
        return [f'AP {i}' for i in range(10)]
