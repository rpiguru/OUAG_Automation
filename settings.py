import os
import shutil

_cur_dir = os.path.dirname(os.path.realpath(__file__))

ROOT_DIR = os.path.expanduser('~/.ouag')
os.makedirs(ROOT_DIR, exist_ok=True)

CONFIG_FILE = os.path.expanduser(os.path.join(ROOT_DIR, 'config.json'))
if not os.path.exists(CONFIG_FILE):
    print('No JSON Config File Found! Recovering the default one...')
    shutil.copy(os.path.join(_cur_dir, 'default_config.json'), CONFIG_FILE)

IMAGE_DIR = os.path.join(ROOT_DIR, 'images')
os.makedirs(IMAGE_DIR, exist_ok=True)

SHOW_CURSOR = False

SCREEN_SIZE = 1024, 600

from PySide2.QtCore import Qt

MONGO_HOST = 'localhost'
MONGO_DB = "ouag"
MONGO_COLLECTION = 'data'

SHOP_URL = "https://onceuponagreen.com/product-category/quickorder/"

PUMP_PINS = {
    'light': 25,
    'exhaust': 12,
    'humidity': 16,
    'air_fan': 20,
    'other': 21
}

SNAP_INTERVALS = [5, 15, 30, 60, 240, 480, 960, 1440, None]

GAUGE_CONF = {
    'ph': {
        'min': 3,
        'max': 11,
        'colors': [[.0, Qt.red], [.39, Qt.yellow], [.47, Qt.green], [.55, Qt.yellow], [.75, Qt.red]],
        'label_count': 8
    },
    'ec': {
        'min': 0,
        'max': 2000,
        'colors': [[.0, Qt.red], [.3, Qt.yellow], [.48, Qt.green], [.55, Qt.yellow], [.75, Qt.red]],
        'label_count': 10
    },
    'temp': {
        'min': 50,
        'max': 100,
        'colors': [[.0, Qt.red], [.23, Qt.yellow], [.3, Qt.green], [.6, Qt.yellow], [.64, Qt.red]],
        'label_count': 10
    },
    'water_temp': {
        'min': 50,
        'max': 100,
        'colors': [[.0, Qt.red], [.23, Qt.yellow], [.3, Qt.green], [.6, Qt.yellow], [.64, Qt.red]],
        'label_count': 10
    },
    'humidity': {
        'min': 0,
        'max': 100,
        'colors': [[.0, Qt.red], [.1, Qt.yellow], [.15, Qt.green], [.6, Qt.yellow], [.65, Qt.red]],
        'label_count': 5
    }
}

try:
    from local_settings import *
except ImportError:
    pass
