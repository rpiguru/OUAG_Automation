# -*- coding: utf-8 -*-

################################################################################
## Form generated from reading UI file 'dlg_ctrl.ui'
##
## Created by: Qt User Interface Compiler version 5.15.1
##
## WARNING! All changes made in this file will be lost when recompiling UI file!
################################################################################

from PySide2.QtCore import *
from PySide2.QtGui import *
from PySide2.QtWidgets import *

import ui.ouag_rc

class Ui_ControlDialog(object):
    def setupUi(self, ControlDialog):
        if not ControlDialog.objectName():
            ControlDialog.setObjectName(u"ControlDialog")
        ControlDialog.resize(682, 419)
        ControlDialog.setStyleSheet(u"#ControlDialog {\n"
"background-color: rgb(214, 234, 182);\n"
"}")
        self.layout_main = QVBoxLayout(ControlDialog)
        self.layout_main.setObjectName(u"layout_main")
        self.layout_main.setContentsMargins(20, 20, 20, 20)
        self.horizontalLayout_2 = QHBoxLayout()
        self.horizontalLayout_2.setObjectName(u"horizontalLayout_2")
        self.label = QLabel(ControlDialog)
        self.label.setObjectName(u"label")
        self.label.setPixmap(QPixmap(u":/img/img/logo_small.png"))

        self.horizontalLayout_2.addWidget(self.label)

        self.horizontalSpacer = QSpacerItem(40, 20, QSizePolicy.Expanding, QSizePolicy.Minimum)

        self.horizontalLayout_2.addItem(self.horizontalSpacer)

        self.verticalLayout = QVBoxLayout()
        self.verticalLayout.setObjectName(u"verticalLayout")
        self.horizontalLayout = QHBoxLayout()
        self.horizontalLayout.setObjectName(u"horizontalLayout")
        self.btn_system_update = QPushButton(ControlDialog)
        self.btn_system_update.setObjectName(u"btn_system_update")
        self.btn_system_update.setMinimumSize(QSize(180, 35))
        font = QFont()
        font.setFamily(u"Comic Sans MS")
        font.setPointSize(12)
        self.btn_system_update.setFont(font)
        self.btn_system_update.setStyleSheet(u"background-color: rgb(112, 182, 3);\n"
"color: rgb(255, 255, 255);\n"
"border-radius: 5px;\n"
"checked { background-color: red;}")

        self.horizontalLayout.addWidget(self.btn_system_update)

        self.btn_software_update = QPushButton(ControlDialog)
        self.btn_software_update.setObjectName(u"btn_software_update")
        self.btn_software_update.setMinimumSize(QSize(180, 35))
        self.btn_software_update.setFont(font)
        self.btn_software_update.setStyleSheet(u"background-color: rgb(112, 182, 3);\n"
"color: rgb(255, 255, 255);\n"
"border-radius: 5px;\n"
"checked { background-color: red;}")

        self.horizontalLayout.addWidget(self.btn_software_update)


        self.verticalLayout.addLayout(self.horizontalLayout)

        self.label_2 = QLabel(ControlDialog)
        self.label_2.setObjectName(u"label_2")
        font1 = QFont()
        font1.setFamily(u"Comic Sans MS")
        font1.setPointSize(9)
        font1.setBold(False)
        font1.setWeight(50)
        self.label_2.setFont(font1)
        self.label_2.setAlignment(Qt.AlignRight|Qt.AlignTrailing|Qt.AlignVCenter)

        self.verticalLayout.addWidget(self.label_2)


        self.horizontalLayout_2.addLayout(self.verticalLayout)


        self.layout_main.addLayout(self.horizontalLayout_2)

        self.widget = QWidget(ControlDialog)
        self.widget.setObjectName(u"widget")
        sizePolicy = QSizePolicy(QSizePolicy.Preferred, QSizePolicy.Expanding)
        sizePolicy.setHorizontalStretch(0)
        sizePolicy.setVerticalStretch(0)
        sizePolicy.setHeightForWidth(self.widget.sizePolicy().hasHeightForWidth())
        self.widget.setSizePolicy(sizePolicy)
        self.layout_button = QHBoxLayout(self.widget)
        self.layout_button.setSpacing(30)
        self.layout_button.setObjectName(u"layout_button")
        self.layout_button.setContentsMargins(0, 0, 0, 0)
        self.lb_control = QLabel(self.widget)
        self.lb_control.setObjectName(u"lb_control")
        sizePolicy1 = QSizePolicy(QSizePolicy.Expanding, QSizePolicy.Preferred)
        sizePolicy1.setHorizontalStretch(0)
        sizePolicy1.setVerticalStretch(0)
        sizePolicy1.setHeightForWidth(self.lb_control.sizePolicy().hasHeightForWidth())
        self.lb_control.setSizePolicy(sizePolicy1)
        self.lb_control.setMaximumSize(QSize(350, 16777215))
        font2 = QFont()
        font2.setPointSize(14)
        self.lb_control.setFont(font2)
        self.lb_control.setAlignment(Qt.AlignRight|Qt.AlignTrailing|Qt.AlignVCenter)

        self.layout_button.addWidget(self.lb_control)

        self.horizontalSpacer_3 = QSpacerItem(20, 20, QSizePolicy.Fixed, QSizePolicy.Minimum)

        self.layout_button.addItem(self.horizontalSpacer_3)


        self.layout_main.addWidget(self.widget)

        self.widget_table = QWidget(ControlDialog)
        self.widget_table.setObjectName(u"widget_table")
        sizePolicy.setHeightForWidth(self.widget_table.sizePolicy().hasHeightForWidth())
        self.widget_table.setSizePolicy(sizePolicy)
        self.layout_table = QVBoxLayout(self.widget_table)
        self.layout_table.setObjectName(u"layout_table")

        self.layout_main.addWidget(self.widget_table)

        self.lb_description = QLabel(ControlDialog)
        self.lb_description.setObjectName(u"lb_description")
        sizePolicy2 = QSizePolicy(QSizePolicy.Preferred, QSizePolicy.Fixed)
        sizePolicy2.setHorizontalStretch(0)
        sizePolicy2.setVerticalStretch(0)
        sizePolicy2.setHeightForWidth(self.lb_description.sizePolicy().hasHeightForWidth())
        self.lb_description.setSizePolicy(sizePolicy2)
        self.lb_description.setMinimumSize(QSize(0, 100))
        font3 = QFont()
        font3.setPointSize(10)
        self.lb_description.setFont(font3)

        self.layout_main.addWidget(self.lb_description)

        self.horizontalLayout_3 = QHBoxLayout()
        self.horizontalLayout_3.setObjectName(u"horizontalLayout_3")
        self.horizontalSpacer_2 = QSpacerItem(40, 20, QSizePolicy.Expanding, QSizePolicy.Minimum)

        self.horizontalLayout_3.addItem(self.horizontalSpacer_2)

        self.btn_close = QPushButton(ControlDialog)
        self.btn_close.setObjectName(u"btn_close")
        self.btn_close.setMinimumSize(QSize(100, 35))
        self.btn_close.setMaximumSize(QSize(100, 16777215))
        self.btn_close.setFont(font)
        self.btn_close.setStyleSheet(u"background-color: rgb(112, 182, 3);\n"
"color: rgb(255, 255, 255);\n"
"border-radius: 5px;\n"
"checked { background-color: red;}")

        self.horizontalLayout_3.addWidget(self.btn_close)


        self.layout_main.addLayout(self.horizontalLayout_3)


        self.retranslateUi(ControlDialog)

        QMetaObject.connectSlotsByName(ControlDialog)
    # setupUi

    def retranslateUi(self, ControlDialog):
        ControlDialog.setWindowTitle("")
        self.label.setText("")
        self.btn_system_update.setText(QCoreApplication.translate("ControlDialog", u"System Update", None))
        self.btn_software_update.setText(QCoreApplication.translate("ControlDialog", u"Software Update", None))
        self.label_2.setText(QCoreApplication.translate("ControlDialog", u"Phone Support: (800) 123-4567     ", None))
        self.lb_control.setText(QCoreApplication.translate("ControlDialog", u"Light:", None))
        self.lb_description.setText("")
        self.btn_close.setText(QCoreApplication.translate("ControlDialog", u"Close", None))
    # retranslateUi

